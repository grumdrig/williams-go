#include <SDL2/SDL.h>

extern void audioCallback(void *userdata, Uint8 * stream, int len);

void SDLCALL sdlAudioCallback(void *userdata, Uint8 * stream, int len) {
  audioCallback(userdata, stream, len);
}
