package sdl

import (
	"unsafe"
	"reflect"
	"fmt"
)

// #cgo LDFLAGS: -framework SDL2
// #include <SDL2/SDL.h>
// extern void SDLCALL sdlAudioCallback(void *userdata, Uint8 * stream, int len);
import "C"

// https://code.google.com/p/go-wiki/wiki/cgo
// http://weekly.golang.org/doc/go1#cmd_cgo

func Init() {
	C.SDL_Init(C.SDL_INIT_EVERYTHING)
}

func Quit() {
	C.SDL_Quit()
}


func GetError() string {
	return C.GoString(C.SDL_GetError())
}

type Window struct {
	p *C.SDL_Window
}

func CreateWindow(title string, w int, h int, fullscreen bool) Window {
 	ctitle := C.CString(title)
    defer C.free(unsafe.Pointer(ctitle))
    flags := C.SDL_WINDOW_INPUT_FOCUS;
    if fullscreen {
    	flags |= C.SDL_WINDOW_FULLSCREEN;
    }
    win := C.SDL_CreateWindow(ctitle, C.SDL_WINDOWPOS_UNDEFINED, C.SDL_WINDOWPOS_UNDEFINED,
    						  C.int(w), C.int(h), C.Uint32(flags))
    if win == nil {
    	panic(GetError())
    }
    return Window { win }
}

type Renderer struct {
	p *C.SDL_Renderer
}

func (win Window) CreateRenderer() Renderer {
	p := C.SDL_CreateRenderer(win.p, -1, C.SDL_RENDERER_PRESENTVSYNC)
    if p == nil {
    	panic(GetError())
    }
	return Renderer { p }
}

func (win *Window) Destroy() {
	C.SDL_DestroyWindow(win.p);
	win.p = nil
}


type Texture struct {
	p *C.SDL_Texture
}

func (renderer Renderer) Clear() {
	if C.SDL_RenderClear(renderer.p) != 0 {
		panic(GetError())
	}
}

func (renderer Renderer) CreateTexture(width int, height int) Texture {
	p := C.SDL_CreateTexture(renderer.p, C.SDL_PIXELFORMAT_ABGR8888,
                                   C.SDL_TEXTUREACCESS_STREAMING,
                                   C.int(width), C.int(height))
	if p == nil { panic(GetError()) }
	return Texture { p }
}

func (renderer Renderer) CopyTexture(texture Texture) {
	r := C.SDL_RenderCopy(renderer.p, texture.p, nil, nil)
	if r != 0 { panic(GetError()) }
}

func (renderer Renderer) Present() {
	C.SDL_RenderPresent(renderer.p)
}

func (texture *Texture) Query() (uint32, int, int, int) {
	var format C.Uint32
	var access C.int
	var w, h C.int
	if C.SDL_QueryTexture(texture.p, &format, &access, &w, &h) != 0 {
		panic(GetError());
	}
	return uint32(format), int(access), int(w), int(h)
}

func (texture *Texture) Lock() (pixels []uint32, pitch int) {
	_, _, _, h := texture.Query()

	var cpitch C.int
	var cpixels unsafe.Pointer
	r := C.SDL_LockTexture(texture.p, nil, &cpixels, &cpitch)
	if r != 0 { panic(GetError()) }

	pitch = int(cpitch) / 4
	pixels = *(*[]uint32)(unsafe.Pointer(&reflect.SliceHeader{
		Data: uintptr(cpixels),
		Len:  pitch * h,
		Cap:  pitch * h,
	}))
	return
}

func (texture *Texture) Unlock() {
	C.SDL_UnlockTexture(texture.p)
}

func (texture *Texture) Update(pixels []uint32, pitch int) {
	r := C.SDL_UpdateTexture(texture.p, nil, unsafe.Pointer(&pixels[0]), C.int(pitch))
	if r != 0 { panic(GetError()) }
}


func Delay(msec uint) {
	C.SDL_Delay(C.Uint32(msec))
}

func GetTicks() uint32 {
	return uint32(C.SDL_GetTicks())
}

type Event struct {
	e C.SDL_Event
}

const (
	QUIT = C.SDL_QUIT
	KEYDOWN = C.SDL_KEYDOWN
	KEYUP = C.SDL_KEYUP
)

func (event *Event) Type() uint32 {
    return *(*uint32)(unsafe.Pointer(event))
}

type KeyboardEvent struct {
	Down	bool
	Sym 	int
}

func (event *Event) KeyboardEvent() KeyboardEvent {
   	ke := (*C.SDL_KeyboardEvent)(unsafe.Pointer(event))
   	return KeyboardEvent { event.Type() == C.SDL_KEYDOWN, int(ke.keysym.sym) }
}

func PollEvent(event *Event) bool {
  return C.SDL_PollEvent(&event.e) != 0
}

// type AudioCallback func (userdata unsafe.Pointer, stream *C.Uint8, len C.int);

//export audioCallback
func audioCallback(userdata unsafe.Pointer, stream *C.Uint8, len C.int) {
	callback := (*func (buffer []byte))(unsafe.Pointer(userdata))
	buffer := *(*[]byte)(unsafe.Pointer(&reflect.SliceHeader{
		Data: uintptr(unsafe.Pointer(stream)),
		Len:  int(len),
		Cap:  int(len),
	}))
	(*callback)(buffer)
}


func OpenAudioDevice(callback *func (buffer []byte)) uint32 {
	var want, have C.SDL_AudioSpec

	want.freq = 8000
	want.format = C.AUDIO_U8
	want.channels = 1
	want.samples = 512
	want.callback = C.SDL_AudioCallback(C.sdlAudioCallback)
	want.userdata = unsafe.Pointer(callback)
	dev := C.SDL_OpenAudioDevice(nil, 0, &want, &have, 0)
	if dev == 0 {
	    panic(GetError())
	}
	fmt.Printf("GOT AUDIO %d %d %d %d\n", have.freq, have.format, have.channels, have.samples)
    C.SDL_PauseAudioDevice(dev, 0)  // start audio playing.
    return uint32(dev)
}

func CloseAudioDevice(id uint32) {
	C.SDL_CloseAudioDevice(C.SDL_AudioDeviceID(id))
}

func LockAudioDevice(id uint32) {
	C.SDL_LockAudioDevice(C.SDL_AudioDeviceID(id))
}

func UnlockAudioDevice(id uint32) {
	C.SDL_UnlockAudioDevice(C.SDL_AudioDeviceID(id))
}

func Example() {
	Init()
	CreateWindow("joy", 304, 256, false)

	for running := true; running; {

		event := C.SDL_Event{}
		for C.SDL_PollEvent(&event) != 0 {
			typ := *(*uint32)(unsafe.Pointer(&event))
			switch typ {
				case C.SDL_QUIT:
					running = false
				case C.SDL_KEYDOWN, C.SDL_KEYUP:
					e := (*C.SDL_KeyboardEvent)(unsafe.Pointer(&event))
					if e.keysym.sym == 27 { running = false }
			}

		}
	}

	Quit()
	println("Bye")
}
