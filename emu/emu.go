package emu

import (
	"io/ioutil"
	"fmt"
	"math/rand"
)


// Represents a destination as determined by the addressing mode of an
// instruction. Either a register, address, or a register pair for TFR or EXG.
type Dest int

func (d Dest) isAddr()	bool { return d >= 0 }
func (d Dest) isReg()	 bool { return !d.isRegPair() && d < 0 }
func (d Dest) isRegPair() bool { return d < 0 && -d & 0xF0 != 0 }

func assert(cond bool) { if !cond { panic("Failed assertion") } }

func (d Dest) addr() uint16 {
	assert(d.isAddr())
	return uint16(d)
}
func (d Dest) asReg() Register {
	assert(d.isReg())
	return Register(-d)
}
func (d Dest) asRegPair() (r1 Register, r2 Register) {
	assert(d.isRegPair())
	r1 = Register(-d >> 4)
	r2 = Register(-d & 0xF)
	return
}

func Address(a uint16) Dest { return Dest(a) }
func Reg(r Register) Dest { return Dest(-r) }
func RegPair(r1, r2 Register) Dest { return Dest(-(r1 << 4 + r2)) }


func s8(u byte) int { return int(int8(u)) }

func s16(u uint16) int { return int(int16(u)) }

type AddrMode int

const (
	INHERENT AddrMode = iota
	IMMEDIATE
	LIMMEDIATE
	DIRECT
	INDEXED
	EXTENDED
	RELATIVE
	LRELATIVE
	REGISTER
)

type FlagSet struct {
	N bool
	Z bool
	V bool
	C bool
}

var (
	noflags = FlagSet { N: false, Z: false, V: false, C: false }
	Z	    = FlagSet { N: false, Z: true,  V: false, C: false }
	NZ	    = FlagSet { N: true,  Z: true,  V: false, C: false }
	NZC	    = FlagSet { N: true,  Z: true,  V: false, C: true  }
	NZV	    = FlagSet { N: true,  Z: true,  V: true,  C: false }
	NZVC	= FlagSet { N: true,  Z: true,  V: true,  C: true  }
)

type ChipName int
const (
	MC6800 = 6800
	MC6802 = 6802
	MC6808 = 6808
	MC6809 = 6809
)

func name(chip ChipName) string {
	return fmt.Sprintf("MC%d", chip)
}

type Legality int
const (
  Legal Legality = iota
  ILLEGAL
  FALLBACK
)

type Register int
const (
	A  Register = iota + 1
	B
	D
	X
	Y
	S
	U
	PC
	DP
	CC
	o  // no register
)

var REGBITS = [...]int{8, 8, 16, 16, 16, 16, 16, 16, 8, 8, 0}

func (self Register) bits() int {
	return REGBITS[self - 1]
}

var REGNAME = [...]string{"A", "B", "D", "X", "Y", "S", "U", "PC", "DP", "CC", "", }

func (self Register) name() string {
	return REGNAME[self - 1]
}


func Inst6800(opcode byte) (mn int, rg Register, cy int, md AddrMode, invalid bool) {
	switch opcode {
		case 0x1B: mn,rg,cy,md = ABA, o, 2, INHERENT
		case 0xB9: mn,rg,cy,md = ADC, A, 4, EXTENDED
		case 0xA9: mn,rg,cy,md = ADC, A, 5, INDEXED
		case 0x99: mn,rg,cy,md = ADC, A, 3, DIRECT
		case 0x89: mn,rg,cy,md = ADC, A, 2, IMMEDIATE
		case 0xF9: mn,rg,cy,md = ADC, B, 4, EXTENDED
		case 0xE9: mn,rg,cy,md = ADC, B, 5, INDEXED
		case 0xD9: mn,rg,cy,md = ADC, B, 3, DIRECT
		case 0xC9: mn,rg,cy,md = ADC, B, 2, IMMEDIATE
		case 0xBB: mn,rg,cy,md = ADD, A, 4, EXTENDED
		case 0xAB: mn,rg,cy,md = ADD, A, 5, INDEXED
		case 0x9B: mn,rg,cy,md = ADD, A, 3, DIRECT
		case 0x8B: mn,rg,cy,md = ADD, A, 2, IMMEDIATE
		case 0xFB: mn,rg,cy,md = ADD, B, 4, EXTENDED
		case 0xEB: mn,rg,cy,md = ADD, B, 5, INDEXED
		case 0xDB: mn,rg,cy,md = ADD, B, 3, DIRECT
		case 0xCB: mn,rg,cy,md = ADD, B, 2, IMMEDIATE
		case 0xB4: mn,rg,cy,md = AND, A, 4, EXTENDED
		case 0xA4: mn,rg,cy,md = AND, A, 5, INDEXED
		case 0x94: mn,rg,cy,md = AND, A, 3, DIRECT
		case 0x84: mn,rg,cy,md = AND, A, 2, IMMEDIATE
		case 0xF4: mn,rg,cy,md = AND, B, 4, EXTENDED
		case 0xE4: mn,rg,cy,md = AND, B, 5, INDEXED
		case 0xD4: mn,rg,cy,md = AND, B, 3, DIRECT
		case 0xC4: mn,rg,cy,md = AND, B, 2, IMMEDIATE
		case 0x78: mn,rg,cy,md = ASL, o, 6, EXTENDED
		case 0x68: mn,rg,cy,md = ASL, o, 7, INDEXED
		case 0x48: mn,rg,cy,md = ASL, A, 2, INHERENT
		case 0x58: mn,rg,cy,md = ASL, B, 2, INHERENT
		case 0x77: mn,rg,cy,md = ASR, o, 6, EXTENDED
		case 0x67: mn,rg,cy,md = ASR, o, 7, INDEXED
		case 0x47: mn,rg,cy,md = ASR, A, 2, INHERENT
		case 0x57: mn,rg,cy,md = ASR, B, 2, INHERENT
		case 0x24: mn,rg,cy,md = BCC, o, 4, RELATIVE
		case 0x25: mn,rg,cy,md = BCS, o, 4, RELATIVE
		case 0x27: mn,rg,cy,md = BEQ, o, 4, RELATIVE
		case 0x2C: mn,rg,cy,md = BGE, o, 4, RELATIVE
		case 0x2E: mn,rg,cy,md = BGT, o, 4, RELATIVE
		case 0x22: mn,rg,cy,md = BHI, o, 4, RELATIVE
		case 0xB5: mn,rg,cy,md = BIT, A, 4, EXTENDED
		case 0xA5: mn,rg,cy,md = BIT, A, 5, INDEXED
		case 0x95: mn,rg,cy,md = BIT, A, 3, DIRECT
		case 0x85: mn,rg,cy,md = BIT, A, 2, IMMEDIATE
		case 0xF5: mn,rg,cy,md = BIT, B, 4, EXTENDED
		case 0xE5: mn,rg,cy,md = BIT, B, 5, INDEXED
		case 0xD5: mn,rg,cy,md = BIT, B, 3, DIRECT
		case 0xC5: mn,rg,cy,md = BIT, B, 2, IMMEDIATE
		case 0x2F: mn,rg,cy,md = BLE, o, 4, RELATIVE
		case 0x23: mn,rg,cy,md = BLS, o, 4, RELATIVE
		case 0x2D: mn,rg,cy,md = BLT, o, 4, RELATIVE
		case 0x2B: mn,rg,cy,md = BMI, o, 4, RELATIVE
		case 0x26: mn,rg,cy,md = BNE, o, 4, RELATIVE
		case 0x2A: mn,rg,cy,md = BPL, o, 4, RELATIVE
		case 0x20: mn,rg,cy,md = BRA, o, 4, RELATIVE
		case 0x8D: mn,rg,cy,md = BSR, o, 8, RELATIVE
		case 0x28: mn,rg,cy,md = BVC, o, 4, RELATIVE
		case 0x29: mn,rg,cy,md = BVS, o, 4, RELATIVE
		case 0x11: mn,rg,cy,md = CBA, o, 2, INHERENT
		case 0x0C: mn,rg,cy,md = CLC, o, 2, INHERENT
		case 0x0E: mn,rg,cy,md = CLI, o, 2, INHERENT
		case 0x7F: mn,rg,cy,md = CLR, o, 6, EXTENDED
		case 0x6F: mn,rg,cy,md = CLR, o, 7, INDEXED
		case 0x4F: mn,rg,cy,md = CLR, A, 2, INHERENT
		case 0x5F: mn,rg,cy,md = CLR, B, 2, INHERENT
		case 0x0A: mn,rg,cy,md = CLV, o, 2, INHERENT
		case 0xB1: mn,rg,cy,md = CMP, A, 4, EXTENDED
		case 0xA1: mn,rg,cy,md = CMP, A, 5, INDEXED
		case 0x91: mn,rg,cy,md = CMP, A, 3, DIRECT
		case 0x81: mn,rg,cy,md = CMP, A, 2, IMMEDIATE
		case 0xF1: mn,rg,cy,md = CMP, B, 4, EXTENDED
		case 0xE1: mn,rg,cy,md = CMP, B, 5, INDEXED
		case 0xD1: mn,rg,cy,md = CMP, B, 3, DIRECT
		case 0xC1: mn,rg,cy,md = CMP, B, 2, IMMEDIATE
		case 0x73: mn,rg,cy,md = COM, o, 6, EXTENDED
		case 0x63: mn,rg,cy,md = COM, o, 7, INDEXED
		case 0x43: mn,rg,cy,md = COM, A, 2, INHERENT
		case 0x53: mn,rg,cy,md = COM, B, 2, INHERENT
		case 0xBC: mn,rg,cy,md = CP,  X, 5, EXTENDED
		case 0xAC: mn,rg,cy,md = CP,  X, 6, INDEXED
		case 0x9C: mn,rg,cy,md = CP,  X, 4, DIRECT
		case 0x8C: mn,rg,cy,md = CP,  X, 3, LIMMEDIATE
		case 0x19: mn,rg,cy,md = DAA, o, 2, INHERENT
		case 0x7A: mn,rg,cy,md = DEC, o, 6, EXTENDED
		case 0x6A: mn,rg,cy,md = DEC, o, 7, INDEXED
		case 0x4A: mn,rg,cy,md = DEC, A, 2, INHERENT
		case 0x5A: mn,rg,cy,md = DEC, B, 2, INHERENT
		case 0x34: mn,rg,cy,md = DE,  S, 4, INHERENT
		case 0x09: mn,rg,cy,md = DE,  X, 4, INHERENT
		case 0xB8: mn,rg,cy,md = EOR, A, 4, EXTENDED
		case 0xA8: mn,rg,cy,md = EOR, A, 5, INDEXED
		case 0x98: mn,rg,cy,md = EOR, A, 3, DIRECT
		case 0x88: mn,rg,cy,md = EOR, A, 2, IMMEDIATE
		case 0xF8: mn,rg,cy,md = EOR, B, 4, EXTENDED
		case 0xE8: mn,rg,cy,md = EOR, B, 5, INDEXED
		case 0xD8: mn,rg,cy,md = EOR, B, 3, DIRECT
		case 0xC8: mn,rg,cy,md = EOR, B, 2, IMMEDIATE
		case 0x7C: mn,rg,cy,md = INC, o, 6, EXTENDED
		case 0x6C: mn,rg,cy,md = INC, o, 7, INDEXED
		case 0x4C: mn,rg,cy,md = INC, A, 2, INHERENT
		case 0x5C: mn,rg,cy,md = INC, B, 2, INHERENT
		case 0x31: mn,rg,cy,md = IN,  S, 4, INHERENT
		case 0x08: mn,rg,cy,md = IN,  X, 4, INHERENT
		case 0x7E: mn,rg,cy,md = JMP, o, 3, EXTENDED
		case 0x6E: mn,rg,cy,md = JMP, o, 4, INDEXED
		case 0xBD: mn,rg,cy,md = JSR, o, 8, EXTENDED
		case 0xAD: mn,rg,cy,md = JSR, o, 9, INDEXED
		case 0xB6: mn,rg,cy,md = LDA, A, 4, EXTENDED
		case 0xA6: mn,rg,cy,md = LDA, A, 5, INDEXED
		case 0x96: mn,rg,cy,md = LDA, A, 3, DIRECT
		case 0x86: mn,rg,cy,md = LDA, A, 2, IMMEDIATE
		case 0xF6: mn,rg,cy,md = LDA, B, 4, EXTENDED
		case 0xE6: mn,rg,cy,md = LDA, B, 5, INDEXED
		case 0xD6: mn,rg,cy,md = LDA, B, 3, DIRECT
		case 0xC6: mn,rg,cy,md = LDA, B, 2, IMMEDIATE
		case 0xBE: mn,rg,cy,md = LD,  S, 5, EXTENDED
		case 0xAE: mn,rg,cy,md = LD,  S, 6, INDEXED
		case 0x9E: mn,rg,cy,md = LD,  S, 4, DIRECT
		case 0x8E: mn,rg,cy,md = LD,  S, 3, LIMMEDIATE
		case 0xFE: mn,rg,cy,md = LD,  X, 5, EXTENDED
		case 0xEE: mn,rg,cy,md = LD,  X, 6, INDEXED
		case 0xDE: mn,rg,cy,md = LD,  X, 4, DIRECT
		case 0xCE: mn,rg,cy,md = LD,  X, 3, LIMMEDIATE
		case 0x74: mn,rg,cy,md = LSR, o, 6, EXTENDED
		case 0x64: mn,rg,cy,md = LSR, o, 7, INDEXED
		case 0x44: mn,rg,cy,md = LSR, A, 2, INHERENT
		case 0x54: mn,rg,cy,md = LSR, B, 2, INHERENT
		case 0x70: mn,rg,cy,md = NEG, o, 6, EXTENDED
		case 0x60: mn,rg,cy,md = NEG, o, 7, INDEXED
		case 0x40: mn,rg,cy,md = NEG, A, 2, INHERENT
		case 0x50: mn,rg,cy,md = NEG, B, 2, INHERENT
		case 0x01: mn,rg,cy,md = NOP, o, 2, INHERENT
		case 0xBA: mn,rg,cy,md = ORA, A, 4, EXTENDED
		case 0xAA: mn,rg,cy,md = ORA, A, 5, INDEXED
		case 0x9A: mn,rg,cy,md = ORA, A, 3, DIRECT
		case 0x8A: mn,rg,cy,md = ORA, A, 2, IMMEDIATE
		case 0xFA: mn,rg,cy,md = ORA, B, 4, EXTENDED
		case 0xEA: mn,rg,cy,md = ORA, B, 5, INDEXED
		case 0xDA: mn,rg,cy,md = ORA, B, 3, DIRECT
		case 0xCA: mn,rg,cy,md = ORA, B, 2, IMMEDIATE
		case 0x36: mn,rg,cy,md = PSH, A, 4, INHERENT
		case 0x37: mn,rg,cy,md = PSH, B, 4, INHERENT
		case 0x32: mn,rg,cy,md = PUL, A, 4, INHERENT
		case 0x33: mn,rg,cy,md = PUL, B, 4, INHERENT
		case 0x79: mn,rg,cy,md = ROL, o,   6, EXTENDED
		case 0x69: mn,rg,cy,md = ROL, o,   7, INDEXED
		case 0x49: mn,rg,cy,md = ROL, A, 2, INHERENT
		case 0x59: mn,rg,cy,md = ROL, B, 2, INHERENT
		case 0x76: mn,rg,cy,md = ROR, o,   6, EXTENDED
		case 0x66: mn,rg,cy,md = ROR, o, 7, INDEXED
		case 0x46: mn,rg,cy,md = ROR, A, 2, INHERENT
		case 0x56: mn,rg,cy,md = ROR, B, 2, INHERENT
		case 0x3B: mn,rg,cy,md = RTI, o,10, INHERENT
		case 0x39: mn,rg,cy,md = RTS, o, 5, INHERENT
		case 0x10: mn,rg,cy,md = SBA, o, 2, INHERENT
		case 0xB2: mn,rg,cy,md = SBC, A, 4, EXTENDED
		case 0xA2: mn,rg,cy,md = SBC, A, 5, INDEXED
		case 0x92: mn,rg,cy,md = SBC, A, 3, DIRECT
		case 0x82: mn,rg,cy,md = SBC, A, 2, IMMEDIATE
		case 0xF2: mn,rg,cy,md = SBC, B, 4, EXTENDED
		case 0xE2: mn,rg,cy,md = SBC, B, 5, INDEXED
		case 0xD2: mn,rg,cy,md = SBC, B, 3, DIRECT
		case 0xC2: mn,rg,cy,md = SBC, B, 2, IMMEDIATE
		case 0x0D: mn,rg,cy,md = SEC, o, 2, INHERENT
		case 0x0F: mn,rg,cy,md = SEI, o, 2, INHERENT
		case 0x0B: mn,rg,cy,md = SEV, o, 2, INHERENT
		case 0xB7: mn,rg,cy,md = STA, A, 5, EXTENDED
		case 0xA7: mn,rg,cy,md = STA, A, 6, INDEXED
		case 0x97: mn,rg,cy,md = STA, A, 4, DIRECT
		case 0xF7: mn,rg,cy,md = STA, B, 5, EXTENDED
		case 0xE7: mn,rg,cy,md = STA, B, 6, INDEXED
		case 0xD7: mn,rg,cy,md = STA, B, 4, DIRECT
		case 0xBF: mn,rg,cy,md = ST,  S, 6, EXTENDED
		case 0xAF: mn,rg,cy,md = ST,  S, 7, INDEXED
		case 0x9F: mn,rg,cy,md = ST,  S, 5, DIRECT
		case 0xFF: mn,rg,cy,md = ST,  X, 6, EXTENDED
		case 0xEF: mn,rg,cy,md = ST,  X, 7, INDEXED
		case 0xDF: mn,rg,cy,md = ST,  X, 5, DIRECT
		case 0xB0: mn,rg,cy,md = SUB, A, 4, EXTENDED
		case 0xA0: mn,rg,cy,md = SUB, A, 5, INDEXED
		case 0x90: mn,rg,cy,md = SUB, A, 3, DIRECT
		case 0x80: mn,rg,cy,md = SUB, A, 2, IMMEDIATE
		case 0xF0: mn,rg,cy,md = SUB, B, 4, EXTENDED
		case 0xE0: mn,rg,cy,md = SUB, B, 5, INDEXED
		case 0xD0: mn,rg,cy,md = SUB, B, 3, DIRECT
		case 0xC0: mn,rg,cy,md = SUB, B, 2, IMMEDIATE
		case 0x3F: mn,rg,cy,md = SWI, o,12, INHERENT
		case 0x16: mn,rg,cy,md = TAB, o, 2, INHERENT
		case 0x06: mn,rg,cy,md = TAP, o, 2, INHERENT
		case 0x17: mn,rg,cy,md = TBA, o, 2, INHERENT
		case 0x07: mn,rg,cy,md = TPA, o, 2, INHERENT
		case 0x7D: mn,rg,cy,md = TST, o, 6, EXTENDED
		case 0x6D: mn,rg,cy,md = TST, o, 6, INDEXED
		case 0x4D: mn,rg,cy,md = TST, A, 2, INHERENT
		case 0x5D: mn,rg,cy,md = TST, B, 2, INHERENT
		case 0x30: mn,rg,cy,md = TSX, o, 4, INHERENT
		case 0x35: mn,rg,cy,md = TXS, o, 4, INHERENT
		case 0x3E: mn,rg,cy,md = WAI, o, 9, INHERENT
		default:   invalid = true
	}
	return
}


func Inst6809(opcode uint16) (mn int,
							  rg Register,
							  mo AddrMode,
							  cy int,
							  by uint16,
							  le Legality,
							  invalid bool) {
	switch opcode {
		case 0x3A  : mn,rg,mo,cy,by,le = ABX,  o  , INHERENT ,	 3,	 1, Legal
		case 0x99  : mn,rg,mo,cy,by,le = ADC,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xB9  : mn,rg,mo,cy,by,le = ADC,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x89  : mn,rg,mo,cy,by,le = ADC,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA9  : mn,rg,mo,cy,by,le = ADC,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xD9  : mn,rg,mo,cy,by,le = ADC,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xF9  : mn,rg,mo,cy,by,le = ADC,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xC9  : mn,rg,mo,cy,by,le = ADC,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE9  : mn,rg,mo,cy,by,le = ADC,  B  , INDEXED  ,	 4,	 2, Legal
		case 0x9B  : mn,rg,mo,cy,by,le = ADD,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xBB  : mn,rg,mo,cy,by,le = ADD,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x8B  : mn,rg,mo,cy,by,le = ADD,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xAB  : mn,rg,mo,cy,by,le = ADD,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xDB  : mn,rg,mo,cy,by,le = ADD,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xFB  : mn,rg,mo,cy,by,le = ADD,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xCB  : mn,rg,mo,cy,by,le = ADD,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xEB  : mn,rg,mo,cy,by,le = ADD,  B  , INDEXED  ,	 4,	 2, Legal
		case 0xD3  : mn,rg,mo,cy,by,le = ADD,  D  , DIRECT   ,	 6,	 2, Legal
		case 0xF3  : mn,rg,mo,cy,by,le = ADD,  D  , EXTENDED ,	 7,	 3, Legal
		case 0xC3  : mn,rg,mo,cy,by,le = ADD,  D  , IMMEDIATE,	 4,	 3, Legal
		case 0xE3  : mn,rg,mo,cy,by,le = ADD,  D  , INDEXED  ,	 6,	 2, Legal
		case 0x94  : mn,rg,mo,cy,by,le = AND,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xB4  : mn,rg,mo,cy,by,le = AND,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x84  : mn,rg,mo,cy,by,le = AND,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA4  : mn,rg,mo,cy,by,le = AND,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xD4  : mn,rg,mo,cy,by,le = AND,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xF4  : mn,rg,mo,cy,by,le = AND,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xC4  : mn,rg,mo,cy,by,le = AND,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE4  : mn,rg,mo,cy,by,le = AND,  B  , INDEXED  ,	 4,	 2, Legal
		case 0x1C  : mn,rg,mo,cy,by,le = AND,  CC , IMMEDIATE,	 3,	 2, Legal
		case 0x38  : mn,rg,mo,cy,by,le = AND,  CC , IMMEDIATE,	 4,	 2, ILLEGAL
		case 0x48  : mn,rg,mo,cy,by,le = ASL,  A  , INHERENT ,	 2,	 1, Legal
		case 0x58  : mn,rg,mo,cy,by,le = ASL,  B  , INHERENT ,	 2,	 1, Legal
		case 0x08  : mn,rg,mo,cy,by,le = ASL,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x78  : mn,rg,mo,cy,by,le = ASL,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x68  : mn,rg,mo,cy,by,le = ASL,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x07  : mn,rg,mo,cy,by,le = ASR,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x77  : mn,rg,mo,cy,by,le = ASR,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x67  : mn,rg,mo,cy,by,le = ASR,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x47  : mn,rg,mo,cy,by,le = ASR,  A  , INHERENT ,	 2,	 1, Legal
		case 0x57  : mn,rg,mo,cy,by,le = ASR,  B  , INHERENT ,	 2,	 1, Legal
		case 0x27  : mn,rg,mo,cy,by,le = BEQ,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x2C  : mn,rg,mo,cy,by,le = BGE,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x2E  : mn,rg,mo,cy,by,le = BGT,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x22  : mn,rg,mo,cy,by,le = BHI,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x24  : mn,rg,mo,cy,by,le = BCC,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x2F  : mn,rg,mo,cy,by,le = BLE,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x25  : mn,rg,mo,cy,by,le = BCS,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x23  : mn,rg,mo,cy,by,le = BLS,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x2D  : mn,rg,mo,cy,by,le = BLT,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x2B  : mn,rg,mo,cy,by,le = BMI,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x26  : mn,rg,mo,cy,by,le = BNE,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x2A  : mn,rg,mo,cy,by,le = BPL,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x20  : mn,rg,mo,cy,by,le = BRA,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x21  : mn,rg,mo,cy,by,le = BRN,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x8D  : mn,rg,mo,cy,by,le = BSR,  o  , RELATIVE ,	 7,	 2, Legal
		case 0x28  : mn,rg,mo,cy,by,le = BVC,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x29  : mn,rg,mo,cy,by,le = BVS,  o  , RELATIVE ,	 3,	 2, Legal
		case 0x95  : mn,rg,mo,cy,by,le = BIT,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xB5  : mn,rg,mo,cy,by,le = BIT,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x85  : mn,rg,mo,cy,by,le = BIT,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA5  : mn,rg,mo,cy,by,le = BIT,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xD5  : mn,rg,mo,cy,by,le = BIT,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xF5  : mn,rg,mo,cy,by,le = BIT,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xC5  : mn,rg,mo,cy,by,le = BIT,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE5  : mn,rg,mo,cy,by,le = BIT,  B  , INDEXED  ,	 4,	 2, Legal
		case 0x0F  : mn,rg,mo,cy,by,le = CLR,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x7F  : mn,rg,mo,cy,by,le = CLR,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x6F  : mn,rg,mo,cy,by,le = CLR,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x4E  : mn,rg,mo,cy,by,le = CLR,  A  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x4F  : mn,rg,mo,cy,by,le = CLR,  A  , INHERENT ,	 2,	 1, Legal
		case 0x5E  : mn,rg,mo,cy,by,le = CLR,  B  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x5F  : mn,rg,mo,cy,by,le = CLR,  B  , INHERENT ,	 2,	 1, Legal
		case 0x91  : mn,rg,mo,cy,by,le = CMP,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xB1  : mn,rg,mo,cy,by,le = CMP,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x81  : mn,rg,mo,cy,by,le = CMP,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA1  : mn,rg,mo,cy,by,le = CMP,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xD1  : mn,rg,mo,cy,by,le = CMP,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xF1  : mn,rg,mo,cy,by,le = CMP,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xC1  : mn,rg,mo,cy,by,le = CMP,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE1  : mn,rg,mo,cy,by,le = CMP,  B  , INDEXED  ,	 4,	 2, Legal
		case 0x9C  : mn,rg,mo,cy,by,le = CMP,  X  , DIRECT   ,	 6,	 2, Legal
		case 0xBC  : mn,rg,mo,cy,by,le = CMP,  X  , EXTENDED ,	 7,	 3, Legal
		case 0x8C  : mn,rg,mo,cy,by,le = CMP,  X  , IMMEDIATE,	 4,	 3, Legal
		case 0xAC  : mn,rg,mo,cy,by,le = CMP,  X  , INDEXED  ,	 6,	 2, Legal
		case 0x1093: mn,rg,mo,cy,by,le = CMP,  D  , DIRECT   ,	 7,	 3, Legal
		case 0x10B3: mn,rg,mo,cy,by,le = CMP,  D  , EXTENDED ,	 8,	 4, Legal
		case 0x1083: mn,rg,mo,cy,by,le = CMP,  D  , IMMEDIATE,	 5,	 4, Legal
		case 0x10A3: mn,rg,mo,cy,by,le = CMP,  D  , INDEXED  ,	 7,	 3, Legal
		case 0x119C: mn,rg,mo,cy,by,le = CMP,  S  , DIRECT   ,	 7,	 3, Legal
		case 0x11BC: mn,rg,mo,cy,by,le = CMP,  S  , EXTENDED ,	 8,	 4, Legal
		case 0x118C: mn,rg,mo,cy,by,le = CMP,  S  , IMMEDIATE,	 5,	 4, Legal
		case 0x11AC: mn,rg,mo,cy,by,le = CMP,  S  , INDEXED  ,	 7,	 3, Legal
		case 0x1193: mn,rg,mo,cy,by,le = CMP,  U  , DIRECT   ,	 7,	 3, Legal
		case 0x11B3: mn,rg,mo,cy,by,le = CMP,  U  , EXTENDED ,	 8,	 4, Legal
		case 0x1183: mn,rg,mo,cy,by,le = CMP,  U  , IMMEDIATE,	 5,	 4, Legal
		case 0x11A3: mn,rg,mo,cy,by,le = CMP,  U  , INDEXED  ,	 7,	 3, Legal
		case 0x109C: mn,rg,mo,cy,by,le = CMP,  Y  , DIRECT   ,	 7,	 3, Legal
		case 0x10BC: mn,rg,mo,cy,by,le = CMP,  Y  , EXTENDED ,	 8,	 4, Legal
		case 0x108C: mn,rg,mo,cy,by,le = CMP,  Y  , IMMEDIATE,	 5,	 4, Legal
		case 0x10AC: mn,rg,mo,cy,by,le = CMP,  Y  , INDEXED  ,	 7,	 3, Legal
		case 0x03  : mn,rg,mo,cy,by,le = COM,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x73  : mn,rg,mo,cy,by,le = COM,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x63  : mn,rg,mo,cy,by,le = COM,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x43  : mn,rg,mo,cy,by,le = COM,  A  , INHERENT ,	 2,	 1, Legal
		case 0x53  : mn,rg,mo,cy,by,le = COM,  B  , INHERENT ,	 2,	 1, Legal
		case 0x3C  : mn,rg,mo,cy,by,le = CWAI, o  , INHERENT ,	21,	 2, Legal
		case 0x19  : mn,rg,mo,cy,by,le = DAA,  o  , INHERENT ,	 2,	 1, Legal
		case 0x0A  : mn,rg,mo,cy,by,le = DEC,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x0B  : mn,rg,mo,cy,by,le = DEC,  o  , DIRECT   ,	 6,	 2, ILLEGAL
		case 0x7A  : mn,rg,mo,cy,by,le = DEC,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x7B  : mn,rg,mo,cy,by,le = DEC,  o  , EXTENDED ,	 7,	 3, ILLEGAL
		case 0x6A  : mn,rg,mo,cy,by,le = DEC,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x6B  : mn,rg,mo,cy,by,le = DEC,  o  , INDEXED  ,	 6,	 2, ILLEGAL
		case 0x4A  : mn,rg,mo,cy,by,le = DEC,  A  , INHERENT ,	 2,	 1, Legal
		case 0x4B  : mn,rg,mo,cy,by,le = DEC,  A  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x5A  : mn,rg,mo,cy,by,le = DEC,  B  , INHERENT ,	 2,	 1, Legal
		case 0x5B  : mn,rg,mo,cy,by,le = DEC,  B  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x98  : mn,rg,mo,cy,by,le = EOR,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xB8  : mn,rg,mo,cy,by,le = EOR,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x88  : mn,rg,mo,cy,by,le = EOR,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA8  : mn,rg,mo,cy,by,le = EOR,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xD8  : mn,rg,mo,cy,by,le = EOR,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xF8  : mn,rg,mo,cy,by,le = EOR,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xC8  : mn,rg,mo,cy,by,le = EOR,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE8  : mn,rg,mo,cy,by,le = EOR,  B  , INDEXED  ,	 4,	 2, Legal
		case 0x1E  : mn,rg,mo,cy,by,le = EXG,  o  , REGISTER ,	 8,	 2, Legal
		case 0x14  : mn,rg,mo,cy,by,le = HCF, o  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x15  : mn,rg,mo,cy,by,le = HCF, o  , INHERENT ,	 2,	 1, ILLEGAL
		case 0xCD  : mn,rg,mo,cy,by,le = HCF, o  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x0C  : mn,rg,mo,cy,by,le = INC,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x7C  : mn,rg,mo,cy,by,le = INC,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x6C  : mn,rg,mo,cy,by,le = INC,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x4C  : mn,rg,mo,cy,by,le = INC,  A  , INHERENT ,	 2,	 1, Legal
		case 0x5C  : mn,rg,mo,cy,by,le = INC,  B  , INHERENT ,	 2,	 1, Legal
		case 0x0E  : mn,rg,mo,cy,by,le = JMP,  o  , DIRECT   ,	 3,	 2, Legal
		case 0x7E  : mn,rg,mo,cy,by,le = JMP,  o  , EXTENDED ,	 3,	 3, Legal
		case 0x6E  : mn,rg,mo,cy,by,le = JMP,  o  , INDEXED  ,	 3,	 2, Legal
		case 0x9D  : mn,rg,mo,cy,by,le = JSR,  o  , DIRECT   ,	 7,	 2, Legal
		case 0xBD  : mn,rg,mo,cy,by,le = JSR,  o  , EXTENDED ,	 8,	 3, Legal
		case 0xAD  : mn,rg,mo,cy,by,le = JSR,  o  , INDEXED  ,	 7,	 2, Legal
		case 0x02  : mn,rg,mo,cy,by,le = LBRA, o  , LRELATIVE,	 5,	 3, ILLEGAL
		case 0x16  : mn,rg,mo,cy,by,le = LBRA, o  , LRELATIVE,	 5,	 3, Legal
		case 0x17  : mn,rg,mo,cy,by,le = LBSR, o  , LRELATIVE,	 9,	 3, Legal
		case 0x1027: mn,rg,mo,cy,by,le = LBEQ, o  , LRELATIVE,	 5,	 4, Legal
		case 0x102C: mn,rg,mo,cy,by,le = LBGE, o  , LRELATIVE,	 5,	 4, Legal
		case 0x102E: mn,rg,mo,cy,by,le = LBGT, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1022: mn,rg,mo,cy,by,le = LBHI, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1024: mn,rg,mo,cy,by,le = LBCC, o  , LRELATIVE,	 5,	 4, Legal
		case 0x102F: mn,rg,mo,cy,by,le = LBLE, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1025: mn,rg,mo,cy,by,le = LBCS, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1023: mn,rg,mo,cy,by,le = LBLS, o  , LRELATIVE,	 5,	 4, Legal
		case 0x102D: mn,rg,mo,cy,by,le = LBLT, o  , LRELATIVE,	 5,	 4, Legal
		case 0x102B: mn,rg,mo,cy,by,le = LBMI, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1026: mn,rg,mo,cy,by,le = LBNE, o  , LRELATIVE,	 5,	 4, Legal
		case 0x102A: mn,rg,mo,cy,by,le = LBPL, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1021: mn,rg,mo,cy,by,le = LBRN, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1028: mn,rg,mo,cy,by,le = LBVC, o  , LRELATIVE,	 5,	 4, Legal
		case 0x1029: mn,rg,mo,cy,by,le = LBVS, o  , LRELATIVE,	 5,	 4, Legal
		case 0x96  : mn,rg,mo,cy,by,le = LD,   A  , DIRECT   ,	 4,	 2, Legal
		case 0xB6  : mn,rg,mo,cy,by,le = LD,   A  , EXTENDED ,	 5,	 3, Legal
		case 0x86  : mn,rg,mo,cy,by,le = LD,   A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA6  : mn,rg,mo,cy,by,le = LD,   A  , INDEXED  ,	 4,	 2, Legal
		case 0xD6  : mn,rg,mo,cy,by,le = LD,   B  , DIRECT   ,	 4,	 2, Legal
		case 0xF6  : mn,rg,mo,cy,by,le = LD,   B  , EXTENDED ,	 5,	 3, Legal
		case 0xC6  : mn,rg,mo,cy,by,le = LD,   B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE6  : mn,rg,mo,cy,by,le = LD,   B  , INDEXED  ,	 4,	 2, Legal
		case 0xDC  : mn,rg,mo,cy,by,le = LD,   D  , DIRECT   ,	 5,	 2, Legal
		case 0xFC  : mn,rg,mo,cy,by,le = LD,   D  , EXTENDED ,	 6,	 3, Legal
		case 0xCC  : mn,rg,mo,cy,by,le = LD,   D  , IMMEDIATE,	 3,	 3, Legal
		case 0xEC  : mn,rg,mo,cy,by,le = LD,   D  , INDEXED  ,	 5,	 2, Legal
		case 0xDE  : mn,rg,mo,cy,by,le = LD,   U  , DIRECT   ,	 5,	 2, Legal
		case 0xFE  : mn,rg,mo,cy,by,le = LD,   U  , EXTENDED ,	 6,	 3, Legal
		case 0xCE  : mn,rg,mo,cy,by,le = LD,   U  , IMMEDIATE,	 3,	 3, Legal
		case 0xEE  : mn,rg,mo,cy,by,le = LD,   U  , INDEXED  ,	 5,	 2, Legal
		case 0x9E  : mn,rg,mo,cy,by,le = LD,   X  , DIRECT   ,	 5,	 2, Legal
		case 0xBE  : mn,rg,mo,cy,by,le = LD,   X  , EXTENDED ,	 6,	 3, Legal
		case 0x8E  : mn,rg,mo,cy,by,le = LD,   X  , IMMEDIATE,	 3,	 3, Legal
		case 0xAE  : mn,rg,mo,cy,by,le = LD,   X  , INDEXED  ,	 5,	 2, Legal
		case 0x108E: mn,rg,mo,cy,by,le = LD,   Y  , IMMEDIATE,	 4,	 4, Legal
		case 0x10DE: mn,rg,mo,cy,by,le = LD,   S  , DIRECT   ,	 6,	 3, Legal
		case 0x10FE: mn,rg,mo,cy,by,le = LD,   S  , EXTENDED ,	 7,	 4, Legal
		case 0x10CE: mn,rg,mo,cy,by,le = LD,   S  , IMMEDIATE,	 4,	 4, Legal
		case 0x10EE: mn,rg,mo,cy,by,le = LD,   S  , INDEXED  ,	 6,	 3, Legal
		case 0x109E: mn,rg,mo,cy,by,le = LD,   Y  , DIRECT   ,	 6,	 3, Legal
		case 0x10BE: mn,rg,mo,cy,by,le = LD,   Y  , EXTENDED ,	 7,	 4, Legal
		case 0x10AE: mn,rg,mo,cy,by,le = LD,   Y  , INDEXED  ,	 6,	 3, Legal
		case 0x32  : mn,rg,mo,cy,by,le = LEA,  S  , INDEXED  ,	 4,	 2, Legal
		case 0x33  : mn,rg,mo,cy,by,le = LEA,  U  , INDEXED  ,	 4,	 2, Legal
		case 0x30  : mn,rg,mo,cy,by,le = LEA,  X  , INDEXED  ,	 4,	 2, Legal
		case 0x31  : mn,rg,mo,cy,by,le = LEA,  Y  , INDEXED  ,	 4,	 2, Legal
		case 0x04  : mn,rg,mo,cy,by,le = LSR,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x05  : mn,rg,mo,cy,by,le = LSR,  o  , DIRECT   ,	 6,	 2, ILLEGAL
		case 0x74  : mn,rg,mo,cy,by,le = LSR,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x75  : mn,rg,mo,cy,by,le = LSR,  o  , EXTENDED ,	 7,	 3, ILLEGAL
		case 0x64  : mn,rg,mo,cy,by,le = LSR,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x65  : mn,rg,mo,cy,by,le = LSR,  o  , INDEXED  ,	 6,	 2, ILLEGAL
		case 0x44  : mn,rg,mo,cy,by,le = LSR,  A  , INHERENT ,	 2,	 1, Legal
		case 0x45  : mn,rg,mo,cy,by,le = LSR,  A  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x54  : mn,rg,mo,cy,by,le = LSR,  B  , INHERENT ,	 2,	 1, Legal
		case 0x55  : mn,rg,mo,cy,by,le = LSR,  B  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x3D  : mn,rg,mo,cy,by,le = MUL,  o  , INHERENT ,	11,	 1, Legal
		case 0x00  : mn,rg,mo,cy,by,le = NEG,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x01  : mn,rg,mo,cy,by,le = NEG,  o  , DIRECT   ,	 6,	 2, ILLEGAL
		case 0x70  : mn,rg,mo,cy,by,le = NEG,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x71  : mn,rg,mo,cy,by,le = NEG,  o  , EXTENDED ,	 7,	 3, ILLEGAL
		case 0x72  : mn,rg,mo,cy,by,le = NECO, o  , EXTENDED ,	 7,	 3, ILLEGAL
		case 0x60  : mn,rg,mo,cy,by,le = NEG,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x61  : mn,rg,mo,cy,by,le = NEG,  o  , INDEXED  ,	 6,	 2, ILLEGAL
		case 0x62  : mn,rg,mo,cy,by,le = NECO, o  , INDEXED  ,	 6,	 2, ILLEGAL
		case 0x40  : mn,rg,mo,cy,by,le = NEG,  A  , INHERENT ,	 2,	 1, Legal
		case 0x41  : mn,rg,mo,cy,by,le = NEG,  A  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x42  : mn,rg,mo,cy,by,le = NECO, A  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x50  : mn,rg,mo,cy,by,le = NEG,  B  , INHERENT ,	 2,	 1, Legal
		case 0x51  : mn,rg,mo,cy,by,le = NEG,  B  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x52  : mn,rg,mo,cy,by,le = NECO, B  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x12  : mn,rg,mo,cy,by,le = NOP,  o  , INHERENT ,	 2,	 1, Legal
		case 0x1B  : mn,rg,mo,cy,by,le = NOP,  o  , INHERENT ,	 2,	 1, ILLEGAL
		case 0x87  : mn,rg,mo,cy,by,le = DROP, o  , IMMEDIATE,	 2,	 2, ILLEGAL
		case 0xC7  : mn,rg,mo,cy,by,le = DROP, o  , IMMEDIATE,	 2,	 2, ILLEGAL
		case 0x9A  : mn,rg,mo,cy,by,le = OR,   A  , DIRECT   ,	 4,	 2, Legal
		case 0xBA  : mn,rg,mo,cy,by,le = OR,   A  , EXTENDED ,	 5,	 3, Legal
		case 0x8A  : mn,rg,mo,cy,by,le = OR,   A  , IMMEDIATE,	 2,	 2, Legal
		case 0xAA  : mn,rg,mo,cy,by,le = OR,   A  , INDEXED  ,	 4,	 2, Legal
		case 0xDA  : mn,rg,mo,cy,by,le = OR,   B  , DIRECT   ,	 4,	 2, Legal
		case 0xFA  : mn,rg,mo,cy,by,le = OR,   B  , EXTENDED ,	 5,	 3, Legal
		case 0xCA  : mn,rg,mo,cy,by,le = OR,   B  , IMMEDIATE,	 2,	 2, Legal
		case 0xEA  : mn,rg,mo,cy,by,le = OR,   B  , INDEXED  ,	 4,	 2, Legal
		case 0x1A  : mn,rg,mo,cy,by,le = OR,   CC , IMMEDIATE,	 3,	 2, Legal
		case 0x34  : mn,rg,mo,cy,by,le = PSHS, o  , IMMEDIATE,	 5,	 2, Legal
		case 0x36  : mn,rg,mo,cy,by,le = PSHU, o  , IMMEDIATE,	 5,	 2, Legal
		case 0x35  : mn,rg,mo,cy,by,le = PULS, o  , IMMEDIATE,	 5,	 2, Legal
		case 0x37  : mn,rg,mo,cy,by,le = PULU, o  , IMMEDIATE,	 5,	 2, Legal
		case 0x3E  : mn,rg,mo,cy,by,le = RESET,o  , INHERENT ,	15,	 1, ILLEGAL
		case 0x09  : mn,rg,mo,cy,by,le = ROL,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x79  : mn,rg,mo,cy,by,le = ROL,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x69  : mn,rg,mo,cy,by,le = ROL,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x49  : mn,rg,mo,cy,by,le = ROL,  A  , INHERENT ,	 2,	 1, Legal
		case 0x59  : mn,rg,mo,cy,by,le = ROL,  B  , INHERENT ,	 2,	 1, Legal
		case 0x06  : mn,rg,mo,cy,by,le = ROR,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x76  : mn,rg,mo,cy,by,le = ROR,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x66  : mn,rg,mo,cy,by,le = ROR,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x46  : mn,rg,mo,cy,by,le = ROR,  A  , INHERENT ,	 2,	 1, Legal
		case 0x56  : mn,rg,mo,cy,by,le = ROR,  B  , INHERENT ,	 2,	 1, Legal
		case 0x3B  : mn,rg,mo,cy,by,le = RTI,  o  , INHERENT ,	 6,	 1, Legal
		case 0x39  : mn,rg,mo,cy,by,le = RTS,  o  , INHERENT ,	 5,	 1, Legal
		case 0x92  : mn,rg,mo,cy,by,le = SBC,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xB2  : mn,rg,mo,cy,by,le = SBC,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x82  : mn,rg,mo,cy,by,le = SBC,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA2  : mn,rg,mo,cy,by,le = SBC,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xD2  : mn,rg,mo,cy,by,le = SBC,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xF2  : mn,rg,mo,cy,by,le = SBC,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xC2  : mn,rg,mo,cy,by,le = SBC,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE2  : mn,rg,mo,cy,by,le = SBC,  B  , INDEXED  ,	 4,	 2, Legal
		case 0x1D  : mn,rg,mo,cy,by,le = SEX,  o  , INHERENT ,	 2,	 1, Legal
		case 0x97  : mn,rg,mo,cy,by,le = ST,   A  , DIRECT   ,	 4,	 2, Legal
		case 0xB7  : mn,rg,mo,cy,by,le = ST,   A  , EXTENDED ,	 5,	 3, Legal
		case 0xA7  : mn,rg,mo,cy,by,le = ST,   A  , INDEXED  ,	 4,	 2, Legal
		case 0xD7  : mn,rg,mo,cy,by,le = ST,   B  , DIRECT   ,	 4,	 2, Legal
		case 0xF7  : mn,rg,mo,cy,by,le = ST,   B  , EXTENDED ,	 5,	 3, Legal
		case 0xE7  : mn,rg,mo,cy,by,le = ST,   B  , INDEXED  ,	 4,	 2, Legal
		case 0xDD  : mn,rg,mo,cy,by,le = ST,   D  , DIRECT   ,	 5,	 2, Legal
		case 0xFD  : mn,rg,mo,cy,by,le = ST,   D  , EXTENDED ,	 6,	 3, Legal
		case 0xED  : mn,rg,mo,cy,by,le = ST,   D  , INDEXED  ,	 5,	 2, Legal
		case 0xDF  : mn,rg,mo,cy,by,le = ST,   U  , DIRECT   ,	 5,	 2, Legal
		case 0xFF  : mn,rg,mo,cy,by,le = ST,   U  , EXTENDED ,	 6,	 3, Legal
		case 0xEF  : mn,rg,mo,cy,by,le = ST,   U  , INDEXED  ,	 5,	 2, Legal
		case 0x9F  : mn,rg,mo,cy,by,le = ST,   X  , DIRECT   ,	 5,	 2, Legal
		case 0xBF  : mn,rg,mo,cy,by,le = ST,   X  , EXTENDED ,	 6,	 3, Legal
		case 0xAF  : mn,rg,mo,cy,by,le = ST,   X  , INDEXED  ,	 5,	 2, Legal
		case 0x10EF: mn,rg,mo,cy,by,le = ST,   S  , INDEXED  ,	 6,	 3, Legal
		case 0x10DF: mn,rg,mo,cy,by,le = ST,   S  , DIRECT   ,	 6,	 3, Legal
		case 0x10FF: mn,rg,mo,cy,by,le = ST,   S  , EXTENDED ,	 7,	 4, Legal
		case 0x109F: mn,rg,mo,cy,by,le = ST,   Y  , DIRECT   ,	 6,	 3, Legal
		case 0x10BF: mn,rg,mo,cy,by,le = ST,   Y  , EXTENDED ,	 7,	 4, Legal
		case 0x10AF: mn,rg,mo,cy,by,le = ST,   Y  , INDEXED  ,	 6,	 3, Legal
		case 0x90  : mn,rg,mo,cy,by,le = SUB,  A  , DIRECT   ,	 4,	 2, Legal
		case 0xB0  : mn,rg,mo,cy,by,le = SUB,  A  , EXTENDED ,	 5,	 3, Legal
		case 0x80  : mn,rg,mo,cy,by,le = SUB,  A  , IMMEDIATE,	 2,	 2, Legal
		case 0xA0  : mn,rg,mo,cy,by,le = SUB,  A  , INDEXED  ,	 4,	 2, Legal
		case 0xD0  : mn,rg,mo,cy,by,le = SUB,  B  , DIRECT   ,	 4,	 2, Legal
		case 0xF0  : mn,rg,mo,cy,by,le = SUB,  B  , EXTENDED ,	 5,	 3, Legal
		case 0xC0  : mn,rg,mo,cy,by,le = SUB,  B  , IMMEDIATE,	 2,	 2, Legal
		case 0xE0  : mn,rg,mo,cy,by,le = SUB,  B  , INDEXED  ,	 4,	 2, Legal
		case 0x93  : mn,rg,mo,cy,by,le = SUB,  D  , DIRECT   ,	 6,	 2, Legal
		case 0xB3  : mn,rg,mo,cy,by,le = SUB,  D  , EXTENDED ,	 7,	 3, Legal
		case 0x83  : mn,rg,mo,cy,by,le = SUB,  D  , IMMEDIATE,	 4,	 3, Legal
		case 0xA3  : mn,rg,mo,cy,by,le = SUB,  D  , INDEXED  ,	 6,	 2, Legal
		case 0x3F  : mn,rg,mo,cy,by,le = SWI,  o  , INHERENT ,	19,	 1, Legal
		case 0x103F: mn,rg,mo,cy,by,le = SWI2, o  , INHERENT ,	20,	 2, Legal
		case 0x113F: mn,rg,mo,cy,by,le = SWI3, o  , INHERENT ,	20,	 2, Legal
		case 0x13  : mn,rg,mo,cy,by,le = SYNC, o  , INHERENT ,	 4,	 1, Legal
		case 0x1F  : mn,rg,mo,cy,by,le = TFR,  o  , REGISTER ,	 7,	 2, Legal
		case 0x0D  : mn,rg,mo,cy,by,le = TST,  o  , DIRECT   ,	 6,	 2, Legal
		case 0x7D  : mn,rg,mo,cy,by,le = TST,  o  , EXTENDED ,	 7,	 3, Legal
		case 0x6D  : mn,rg,mo,cy,by,le = TST,  o  , INDEXED  ,	 6,	 2, Legal
		case 0x4D  : mn,rg,mo,cy,by,le = TST,  A  , INHERENT ,	 2,	 1, Legal
		case 0x5D  : mn,rg,mo,cy,by,le = TST,  B  , INHERENT ,	 2,	 1, Legal
		case 0x1010: mn,rg,mo,cy,by,le = TRACE,o  , INHERENT ,	 2,	 2, ILLEGAL
		case 0x1011: mn,rg,mo,cy,by,le = WTF,  o  , INHERENT ,	 2,	 2, ILLEGAL
		case 0x1110: mn,rg,mo,cy,by,le = WTF,  o  , INHERENT ,	 2,	 2, ILLEGAL
		case 0x1111: mn,rg,mo,cy,by,le = WTF,  o  , INHERENT ,	 2,	 2, ILLEGAL
		case 0x18  : mn,rg,mo,cy,by,le = SCC,  o  , INHERENT ,	 3,	 1, ILLEGAL
		default:
			if opcode >> 8 == 0x10 || opcode >> 8 == 0x11 {
				mn,rg,mo,cy,by,_,invalid = Inst6809(opcode & 0xFF)
				le = FALLBACK
			} else {
				invalid = true
			}
	}
	return
}


type Cpu struct {
	E bool
	F bool
	H bool
	I bool
	N bool
	Z bool
	V bool
	C bool

	A byte
	B byte
	DP byte
	X uint16
	Y uint16
	S uint16
	U uint16
	PC uint16

	sInitialized bool

	CLOCK int
	MCLOCK int

	Verbose bool

	partNumber ChipName
	clockRate float64
	stackAdjust uint16

	RESET bool
	NMI bool
	IRQ bool
	FIRQ bool
	WAIT bool

	SYNC bool

	rom map[uint16][]byte
	Ram []byte
	io map[uint16]byte
	ioStart uint16
	ioLength uint16

	pias []*Pia6821

	writeHooks map[uint16]func(byte,uint16)
	readHooks  map[uint16]func(uint16)
}


func bit(b bool) byte { if b {return 1} else {return 0} }
func bit16(b bool) uint16 { if b {return 1} else {return 0} }

// S is considered "uninitialized" when it has this value. NMIs are disabled
// until such time as S is initialized. This is cheesy, but, will only fail if
// S happens to have this value when a NMI is triggered for the first time.
const UNINITIALIZED uint16 = 0xEEEE


func CpuNew(part ChipName, rate float64) *Cpu {
	cpu := Cpu {
		S: UNINITIALIZED,
		partNumber: part,
		clockRate: rate,
		RESET: true,
		rom: map[uint16][]byte{},
		Ram: []byte{},
		io: map[uint16]byte{},
		pias: []*Pia6821{},
		writeHooks: map[uint16]func(byte,uint16){},
		readHooks:  map[uint16]func(uint16){},
	}
	if part != MC6809 { cpu.stackAdjust = 1 } // 6800 stack points below last push, 6809 points at it
	return &cpu
}

func (self *Cpu) CC() byte {
	return (bit(self.E) << 7) | (bit(self.F) << 6) | (bit(self.H) << 5) | (bit(self.I) << 4) |
		   (bit(self.N) << 3) | (bit(self.Z) << 2) | (bit(self.V) << 1) |  bit(self.C)
}

func (self *Cpu) set_CC(v byte) {
	self.E = 1 == 1 & (v >> 7)
	self.F = 1 == 1 & (v >> 6)
	self.H = 1 == 1 & (v >> 5)
	self.I = 1 == 1 & (v >> 4)
	self.N = 1 == 1 & (v >> 3)
	self.Z = 1 == 1 & (v >> 2)
	self.V = 1 == 1 & (v >> 1)
	self.C = 1 == 1 & v
}

func (self *Cpu) D() uint16 {
	return (uint16(self.A) << 8) + uint16(self.B)
}

func (self *Cpu) set_D(v uint16) {
	self.A = byte(v >> 8)
	self.B = byte(v)
}

func (self *Cpu) traceBytes(start uint16, bytes uint16, width int, sep string) string {
	if width == 0 { width = int(bytes) }
	bs := ""
	for i := 0; i < width; i++ {
		if i < int(bytes) {
			bs += fmt.Sprintf("%02X", self.fetch8(start + uint16(i), true, false))
		} else {
			bs += "  "
		}
		bs += sep
	}
	return bs
}

func (self *Cpu) disassemble6800(pc uint16, withState bool) (asm string, nbytes uint16) {
	opcode := self.fetch8(pc, true, false)
	nbytes = 1
	mn, register, _, mode, invalid := Inst6800(opcode)
	if invalid {
		self.error(fmt.Sprintf("Invalid opcode %02X", opcode))
	}

	mnemonic := Mnemonic(mn) + register.name()

	var operand string

	switch mode {
		case INHERENT: {
			operand = ""
		}
		case RELATIVE: {
			nbytes = 2
			addr := uint16(int(pc) + 2 + s8(self.fetch8(pc + 1, true, false)))
			operand = fmt.Sprintf("L_%04X", addr)
		}
		case EXTENDED: {
			nbytes = 3
			addr := self.fetch16(pc + 1, true)
			operand = fmt.Sprintf("$%04X", addr)
		}
		case DIRECT: {
			nbytes = 2
			addr := uint16(self.fetch8(pc + 1, true, false))
			operand = fmt.Sprintf("<$%04X", addr)
		}
		case INDEXED: {
			nbytes = 2
			var a = self.fetch8(pc + 1, true, false)
			operand = fmt.Sprintf("X + $%02X", a)
		}
		case LIMMEDIATE: {
			addr := pc + 1
			nbytes = 3
			operand = fmt.Sprintf("#$%04X", self.fetch16(addr, true))
		}
		case IMMEDIATE: {
			addr := pc + 1
			nbytes = 2
			operand = fmt.Sprintf("#$%02X", self.fetch8(addr, true, false))
		}
		case LRELATIVE, REGISTER: panic("Impossible addressing mode for 6800")
	}

	var bytes = self.traceBytes(pc, nbytes, 4, " ")
	state := ""
	if withState { state = self.state() + " " }
	asm = fmt.Sprintf("%04X: %s%s %s %s", uint(pc), state, bytes, mnemonic, operand)
	return
}

func (self *Cpu) state() string {
	flg := func (v bool, f string) string { if v {return f} else {return "-"} }
	if self.partNumber == MC6809 {
		stack := self.traceBytes(self.S + self.stackAdjust, 2, 2, ",")
		return fmt.Sprintf("<%02X %02X:%02X x%04X y%04X s%04X[%s] u%04X %s%s%s%s%s%s%s%s",
						   uint(self.DP), uint(self.A), uint(self.B),
						   uint(self.X), uint(self.Y), uint(self.S),
						   stack, uint(self.U),
						   flg(self.E, "E"),
						   flg(self.F, "F"),
						   flg(self.H, "H"),
						   flg(self.I, "I"),
						   flg(self.N, "N"),
						   flg(self.Z, "Z"),
						   flg(self.V, "V"),
						   flg(self.C, "C"))
	} else {
		stack := self.traceBytes(self.S + self.stackAdjust, 4, 4, ",")
		return fmt.Sprintf("%02X:%02X <%04X %04X[%s] %s%s%s%s%s%s",
						   self.A,
						   self.B,
						   self.X,
						   self.S,
						   stack,
						   flg(self.H, "H"),
						   flg(self.I, "I"),
						   flg(self.N, "N"),
						   flg(self.Z, "Z"),
						   flg(self.V, "V"),
						   flg(self.C, "C"))
	}
}



func (self *Cpu) disassemble6809(pc uint16, withState bool) (asm string, nbytes uint16) {
	var opcode = uint16(self.fetch8(pc, true, false))
	var ppc = pc + 1
	if opcode == 0x10 || opcode == 0x11 {
		opcode = opcode * 256 + uint16(self.fetch8(ppc, true, false))
		ppc += 1
	}

	mn, register, mode, _, nb, _, invalid := Inst6809(opcode)
	if invalid {
		self.error(fmt.Sprintf("Invalid opcode %02X", opcode))
	}

	nbytes = nb
	var mnemonic = Mnemonic(mn) + register.name()

	var width = register.bits()

	var param string
	switch mode {
		case EXTENDED:
			param = fmt.Sprintf("$%04X", uint(self.fetch16(ppc, true)))

		case INHERENT:
			param = ""

		case RELATIVE:
			param = fmt.Sprintf("L_%04X", int(ppc + 1 + uint16(s8(self.fetch8(ppc, true, false)))))

		case LRELATIVE:
			param = fmt.Sprintf("L_%04X", int(ppc + 2 + uint16(s16(self.fetch16(ppc, true)))))

		case DIRECT:
			offset := self.fetch8(ppc, true, false)
			addr := uint16(self.DP) * 256 + uint16(offset)
			param = fmt.Sprintf("DP:$%02X ; %04X", uint(offset), uint(addr))

		case REGISTER:
			var TFR_REGS = [...]string{"D", "X", "Y",  "U",  "S",  "PC", "??", "??",
									   "A", "B", "CC", "DP", "??", "??", "??", "??"}
			var postbyte = self.fetch8(ppc, true, false)
			param = fmt.Sprintf("%s, %s", TFR_REGS[postbyte & 0xF], TFR_REGS[postbyte >> 4])

		case IMMEDIATE:
			if width == 16 {
				param = fmt.Sprintf("#$%04X", self.fetch16(ppc, true))
			} else {
				param = fmt.Sprintf("#$%02X", self.fetch8(ppc, true, false))
			}

		case INDEXED:
			// For this mode we"ll update ppc in order to correct the value of nbytes
			var postbyte = self.fetch8(ppc, true, false)
			ppc += 1
			var n = uint(postbyte >> 5) & 0x3
			var subregister = [...]Register{X,Y,U,S}[n]
			var indirect = ((postbyte & 0x10) != 0)
			var submode = postbyte & 0xF
			if postbyte == 0x9F {
				// Extended indirect
				var ptr = self.fetch16(ppc, true)
				ppc += 2
				var a = self.fetch16(ptr, true)
				param = fmt.Sprintf("[%04X] ; %04X", uint(ptr), uint(a))
			} else if (postbyte & 0x80) == 0 {
				// 5-bit offset from register contained in postbyte
				isubmode := int(submode)
				if indirect { isubmode -= 0x10 }  // account for sign bit
				var a = uint16(int(self.reg(subregister)) + isubmode)
				param = fmt.Sprintf("%s,%d ; %04X", subregister.name(), isubmode, uint(a))
			} else {
				var addr = self.reg(subregister)
				param = "" + subregister.name()
				var offset int
				if (submode == 0x4) {
					offset = 0
				} else if (submode == 0x8) {
					offset = s8(self.fetch8(ppc, true, false))
					ppc += 1
				} else if (submode == 0x9) {
					offset = s16(self.fetch16(ppc, true))
					ppc += 2
				} else if (submode == 0x6) {
					param = subregister.name() + ",A"
					offset = int(self.A)
				} else if (submode == 0x5) {
					param = subregister.name() + ",B"
					offset = int(self.B)
				} else if (submode == 0xB) {
					param = subregister.name() + ",D"
					offset = int(self.D())
				} else if (submode == 0) {
					param = param + "+"
					offset = 0
				} else if (submode == 1) {
					param = param + "++"
					offset = 0
				} else if (submode == 2) {
					param = "-" + param
					offset = -1	// it"s a pre-decrement
				} else if (submode == 3) {
					param = "--" + param
					offset = -2	// it"s a pre-decrement
				} else if (submode == 0xC) {
					addr = 0
					offset = s8(self.fetch8(ppc, true, false))
					ppc += 1
					param = fmt.Sprintf("%d", offset)
				} else if (submode == 0xD) {
					addr = 0
					offset = s16(self.fetch16(ppc, true))
					ppc += 2
					param = fmt.Sprintf("%d", offset)
				} else {
					param = "??"
					offset = 0
				}

				if subregister.name() == param && offset != 0 {
					param = fmt.Sprintf("%s,%d", param, offset)
				}

				addr = uint16(int(addr) + offset)
				param = fmt.Sprintf("%s ; %04X", param, uint(addr))

				if (indirect) {
					addr = self.fetch16(addr, true)
					param = fmt.Sprintf("[%s] ; %04X", param, uint(addr))
				}
			}
			nbytes = ppc - pc

		case LIMMEDIATE:
			panic("Impossible addressing mode")
	}


	bytes := self.traceBytes(pc, nbytes, 5, " ")

	state := ""
	if withState { state = self.state() + " " }
	asm = fmt.Sprintf("%04X: %s%s%s %s", pc, state, bytes, mnemonic, param)
	return
}


func (self *Cpu) UnloadRom(addr uint16) {
	delete(self.rom, addr)
}

func (self *Cpu) LoadRom(addr uint16, bytes []byte) {
	self.rom[addr] = bytes
}

func (self *Cpu) DefineIO(start uint16, length uint16) {
	self.ioStart = start
	self.ioLength = length
}

/* TODO get to this later
func (self *Cpu) loadSrec(file:[]byte) {
	// http://en.wikipedia.org/wiki/SREC_(file_format)
	for i in range(0, len(file)) {
		returnfile[i]
		var command = line.substr(0,2)
		if (command == "S9") { break }
		if (command != "S1") {
			self.error("Unrecognized SREC record type: " + command)
		}
		var bytecount = parseInt(line.substr(2,2), 16)
		var address = parseInt(line.substr(4,4), 16)
		var bytes = []
		for j in range(0, bytecount - 3) {
			var v = parseInt(line.substr(8 + j * 2, 2), 16)
			self.store8(address, v)
			address += 1
		}
	}
}
*/

func (self *Cpu) LoadRam(addr uint16, data []byte) {
	assert(addr == 0) // Unimplected RAM elsewhere than at 0
	self.Ram = data
}

func (self *Cpu) CreateRam(addr uint16, length uint) {
	assert(addr == 0) // Unimplected: RAM elsewhere than at 0
	self.Ram = make([]byte, length)
}

func (self *Cpu) randRam(addr uint16, length int) {
	if length == 0 { length = len(self.Ram) - int(addr) }
	for i := 0; i < length; i++ {
		self.Ram[addr + uint16(i)] = byte(rand.Uint32())
	}
}


func (self *Cpu) error(msg string) {
	panic(fmt.Sprintf("ERROR [%s:%04X]: %s", name(self.partNumber), uint(self.PC), msg))
}

func (self *Cpu) warning(msg string) {
	fmt.Printf("WARNING [%s:%4X]: %s\n", name(self.partNumber), uint(self.PC), msg)
}

func (self *Cpu) fetch8(address uint16, noerror bool, norom bool) byte {
	// if (!noerror && self.readHooks.has_key(address)) {
	//	 self.readHooks[address](address)
	// }
	if !norom {
		for offset, rom := range self.rom {
			var delta = address - offset
			// fmt.Printf("FETCH8 %x %x[%d] %x\n", address, offset, len(rom), address - offset)
			if delta < uint16(len(rom)) {
				return rom[delta]
			}
		}
	}

	if int(address) < len(self.Ram) {
		return self.Ram[address]
	}

	for i := 0; i < len(self.pias); i++ {
		var pia = self.pias[i]
		var offset = address - pia.address
		if offset < 4 {
			return pia.read(offset)
		}
	}

	if address >= self.ioStart && address < self.ioStart + self.ioLength {
		return self.io[address]
	}

	if !noerror {
		self.warning(fmt.Sprintf("Read from invalid address $%04X", address))
	}

	return 0
}

func (self *Cpu) fetch16(address uint16, noerr bool) uint16 {
	return uint16(self.fetch8(address, noerr, false)) * 256 +
		   uint16(self.fetch8(address + 1, noerr, false))
}

func (self *Cpu) FetchRam(address uint16) byte { return self.Ram[address] }

func (self *Cpu) nextByte() byte { self.PC += 1; return self.fetch8(self.PC - 1, false, false) }

func (self *Cpu) nextWord() uint16 { self.PC += 2; return self.fetch16(self.PC - 2, false) }

// Some friendlier accessors. Nomenclature is not that great.
func (self *Cpu) Store(dest uint16, value byte) {
	self.store8(Address(dest), int(value), noflags)
}
func (self *Cpu) Storew(dest uint16, value uint16) {
	self.store16(Address(dest), int(value), noflags)
}
func (self *Cpu) Fetch(addr uint16) byte { return self.fetch8(addr, false, false) }
func (self *Cpu) Fetchw(addr uint16) uint16 { return self.fetch16(addr, false) }

func (self *Cpu) store8(dest Dest, value int, flags FlagSet) {
	var uvalue = byte(value)
	if dest.isReg() {
		reg := dest.asReg()
		switch reg {
			case CC: self.set_CC(uvalue)
			case A : self.A = uvalue
			case B : self.B = uvalue
			case DP: self.DP = uvalue
			default : self.error("Invalid store8 destination " + reg.name())
		}
	} else {
		addr := dest.addr()
		if int(addr) < len(self.Ram) {
			self.Ram[addr] = uvalue
		} else {
			var written = false
			for _, pia := range self.pias {
				var offset = addr - pia.address
				if offset < 4 {
					pia.write(offset, uvalue)
					written = true
					break
				}
			}
			if !written {
				if addr >= self.ioStart &&
				   addr < self.ioStart + self.ioLength {
					self.io[addr] = uvalue
				} else {
					self.warning(fmt.Sprintf("Invalid store to address $%04X", addr))
				}
			}
		}
		if hook,ok := self.writeHooks[addr]; ok {
			hook(uvalue, addr)
		}
	}
	if flags != noflags {
		self.tst8(flags, value)
	}
}

func (self *Cpu) store16(dest Dest, value int, flags FlagSet) {
	uvalue := uint16(value)
	if dest.isAddr() {
		addr := dest.addr()
		self.store8(dest, int(value >> 8), noflags)
		self.store8(Address(addr+1), int(byte(value)), noflags)
	} else {
		reg := dest.asReg()
		switch reg {
			case D : self.set_D(uvalue)
			case X : self.X = uvalue
			case Y : self.Y = uvalue
			case S : self.S = uvalue
			case U : self.U = uvalue
			case PC: self.PC = uvalue
			default : self.error("Invalid store16 destination " + reg.name())
		}
	}
	if flags != noflags {
		self.tst16(flags, value)
	}
}

func (self *Cpu) patch(address uint16, value byte) {
	for offset, rom := range self.rom {
		var delta = address - offset
		if int(delta) < len(rom) {
			rom[delta] = value
			return
		}
	}
	self.error("No rom for patch")
}

func (self *Cpu) tst8(flags FlagSet, value int) {
	if flags.N {
		self.N = 1 == 1 & (value >> 7)
	}
	if flags.Z {
		self.Z = 0 == (value & 0xFF)
	}
	if flags.V {
		self.V = (value < -0x80 || value > 0x7F)
	}
	if flags.C {
		self.C = 1 == 1 & (value >> 8)
	}
}

func (self *Cpu) tst16(flags FlagSet, value int) {
	if flags.N {
		self.N = 1 == 1 & (value >> 15)
	}
	if flags.Z {
		self.Z = 0 == (value & 0xFFFF)
	}
	if flags.V {
		self.V = value < -0x8000 || value > 0x7FFF
	}
	if flags.C {
		self.C = 1 == 1 & (value >> 16)
	}
}


func (self *Cpu) reg(r Register) uint16 {
	switch r {
		case CC: return uint16(self.CC())
		case A : return uint16(self.A)
		case B : return uint16(self.B)
		case D : return self.D()
		case DP: return uint16(self.DP)
		case X : return self.X
		case Y : return self.Y
		case S : return self.S
		case U : return self.U
		case PC: return self.PC
		default : self.error(fmt.Sprintf("Invalid reg name %d '%s'", r, r.name())); return 0
	}
}

func (self *Cpu) set_reg(r Register, v uint16) {
	switch r {
		case D : self.set_D(v)
		case X : self.X = v
		case Y : self.Y = v
		case S : self.S = v
		case U : self.U = v
		case PC: self.PC = v
		default : self.set_reg8(r, byte(v))
	}
}

func (self *Cpu) set_reg8(r Register, v byte) {
	switch r {
		case CC: self.set_CC(v)
		case A : self.A = v
		case B : self.B = v
		case DP: self.DP = v
		default : self.error(fmt.Sprintf("Invalid reg name '%s'", r.name()))
	}
}

func (self *Cpu) push8(value byte, stack Register) {
	var s = self.reg(stack) - 1
	self.set_reg(stack, s)
	self.store8(Address(s + self.stackAdjust), int(value), noflags)
}
func (self *Cpu) push16(value uint16, stack Register) {
	var s = self.reg(stack) - 2
	self.set_reg(stack, s)
	self.store16(Address(s + self.stackAdjust), int(value), noflags)
}

func (self *Cpu) pull8(stack Register) byte {
	var s = self.reg(stack)
	self.set_reg(stack, s + 1)
	return self.fetch8(s + self.stackAdjust, false, false)
}
func (self *Cpu) pull16(stack Register) uint16 {
	var s = self.reg(stack)
	self.set_reg(stack, s + 2)
	return self.fetch16(s + self.stackAdjust, false)
}

// Unsigned 2"s complement adds
func (self *Cpu) add8(v1 byte, v2 byte, c byte, flags FlagSet) byte {
	var sum = int(v1) + int(v2) + int(c)
	var result = byte(sum)
	if flags.N {
		self.N = 1 == 1 & (result >> 7)
	}
	if flags.Z {
		self.Z = result == 0
	}
	if flags.V {
		self.V = 1 == 1 & ((int(v1) ^ int(v2) ^ sum ^ (sum >> 1)) >> 7)
	}
	if flags.C {
		self.C = 1 == 1 & (sum >> 8)
	}
	return result
}

func (self *Cpu) add16(v1 uint16, v2 uint16, c uint16, flags FlagSet) uint16 {
	var sum = int(v1) + int(v2) + int(c)
	var result = uint16(sum)
	if (flags.N) {
		self.N = 1 == 1 & (result >> 15)
	}
	if (flags.Z) {
		self.Z = result == 0
	}
	if (flags.V) {
		self.V = 1 == 1 & ((int(v1) ^ int(v2) ^ sum ^ (sum >> 1)) >> 15)
	}
	if (flags.C) {
		self.C = 1 == 1 & (sum >> 16)
	}
	return result
}

func (self *Cpu) sub8(v1 byte, v2 byte, c byte, flags FlagSet) byte {
	var sum = int(v1) - int(v2) - int(c)
	var result = byte(sum)
	if flags.N {
		self.N = 1 == 1 & (result >> 7)
	}
	if flags.Z {
		self.Z = result == 0
	}
	if flags.V {
		self.V = 1 == 1 & ((int(v1) ^ int(v2) ^ sum ^ (sum >> 1)) >> 7)
	}
	if flags.C {
		self.C = 1 == 1 & (sum >> 8)
	}
	return result
}

func (self *Cpu) sub16(v1 uint16, v2 uint16, c uint16, flags FlagSet) uint16 {
	var sum = int(v1) - int(v2) - int(c)
	var result = uint16(sum)
	if flags.N {
		self.N = 1 == 1 & (result >> 15)
	}
	if flags.Z {
		self.Z = result == 0
	}
	if flags.V {
		self.V = 1 == 1 & ((int(v1) ^ int(v2) ^ sum ^ (sum >> 1)) >> 15)
	}
	if flags.C {
		self.C = 1 == 1 & (sum >> 16)
	}
	return result
}

func (self *Cpu) tstH(v1 uint16, v2 uint16, c byte) {
	self.H = 1 == (((0xF & v1) + (0xF & v2) + uint16(c)) >> 4) & 1
}


func (self *Cpu) step6800() bool {
	if (self.RESET) {
		self.Reset()
		self.elapse(1) // ?
		return false
	}
	self.sInitialized = self.sInitialized || self.S != UNINITIALIZED
	if self.NMI && self.sInitialized {
		self.nmi6800()
		self.elapse(1) // ?
		return false
	}
	if self.IRQ && !self.I {
		self.irq6800()
		self.elapse(1) // ?
		return false
	}
	if self.WAIT {
		self.elapse(1)
		return false
	}

	var opPC = self.PC

	var opcode = self.nextByte()
	mnemonic, register, cycles, addrmode, invalid := Inst6800(opcode)
	if invalid {
		self.error(fmt.Sprintf("Invalid opcode %02X", opcode))
	}

	self.elapse(cycles)

	var width = register.bits()

	var addr Dest
	switch addrmode {
		case DIRECT	: addr = Address(uint16(self.nextByte()))
		case EXTENDED  : addr = Address(self.nextWord())
		case IMMEDIATE : self.PC += 1; addr = Address(self.PC - 1)
		case LIMMEDIATE: self.PC += 2; addr = Address(self.PC - 2)
		case INDEXED   : addr = Address(self.X + uint16(self.nextByte()))
		case RELATIVE  : addr = Address(self.PC + uint16(s8(self.nextByte())))
		case INHERENT  : addr = Reg(register)
		default		   : panic(fmt.Sprintf("Invalid addressing mode %d", int(addrmode)))
	}

	var R uint16
	if register != o { R = self.reg(register) }

	store := func(a Dest, v int, f FlagSet) { if width == 16 { self.store16(a,v,f) } else { self.store8(a,v,f) } }
	add := func(a uint16, b uint16, c uint16, f FlagSet) uint16 { if width == 16 { return self.add16(a,b,c,f) } else { return uint16(self.add8(byte(a),byte(b),byte(c),f)) } }
	sub := func(a uint16, b uint16, c uint16, f FlagSet) uint16 { if width == 16 { return self.sub16(a,b,c,f) } else { return uint16(self.sub8(byte(a),byte(b),byte(c),f)) } }
	tst := func(f FlagSet, a int) { if width == 16 { self.tst16(f,a) } else { self.tst8(f,a) } }

	var fetch func() uint16;
	fetch = func() uint16 {  // call only once!
		fetch = func() uint16 { panic("Call only once") }
		if addr.isReg() {
			return R
		} else if width == 16 {
		   return self.fetch16(addr.addr(), false)
		} else {
		   return uint16(self.fetch8(addr.addr(), false, false))
		}
	}

	switch mnemonic {
	case ABA:
		self.tstH(uint16(self.A), uint16(self.B), 0)
		self.A = self.add8(self.A, self.B, 0, NZVC)

	case ADC:
		var v = fetch()
		self.tstH(R, v, bit(self.C))
		store(Reg(register), int(add(R, v, bit16(self.C), NZVC)), noflags)

	case ADD:
		var v = fetch()
		self.tstH(R, v, 0)
		store(Reg(register), int(add(R, v, 0, NZVC)), noflags)

	case AND:
		flags := NZV
		if register == CC { flags = noflags }
		store(Reg(register), int(R & fetch()), flags)

	case ASL:
		var v = fetch()
		store(addr, int(add(v, v, 0, NZVC)), noflags)

	case ASR:
		var v = fetch()
		store(addr, int(v >> 1), NZ)
		self.C = 1 == v & 1
		self.V = self.N != self.C

	case BCC:
		if !self.C { self.PC = addr.addr() }
	case BCS:
		if self.C { self.PC = addr.addr() }
	case BEQ:
		if self.Z { self.PC = addr.addr() }
	case BGE:
		if self.N == self.V { self.PC = addr.addr() }
	case BGT:
		if self.N == self.V && !self.Z { self.PC = addr.addr() }
	case BHI:
		if !self.Z && !self.C { self.PC = addr.addr() }
	case BLE:
		if self.N != self.V || self.Z { self.PC = addr.addr() }
	case BLS:
		if self.Z || self.C { self.PC = addr.addr() }
	case BLT:
		if self.N != self.V { self.PC = addr.addr() }
	case BMI:
		if self.N { self.PC = addr.addr() }
	case BNE:
		if !self.Z { self.PC = addr.addr() }
	case BPL:
		if !self.N { self.PC = addr.addr() }
	case BRA:
		self.PC = addr.addr()
	case BSR:
		self.push16(self.PC, S)
		self.PC = addr.addr()
	case BVC:
		if !self.V { self.PC = addr.addr() }
	case BVS:
		if self.V { self.PC = addr.addr() }

	case BIT:
		tst(NZV, int(R & fetch()))

	case CBA:
		self.sub8(self.A, self.B, 0, NZVC)

	case CLC:
		self.C = false

	case CLI:
		self.I = false

	case CLR:
		store(addr, 0, noflags)
		self.N = false
		self.V = false
		self.C = false
		self.Z = true

	case CLV:
		self.V = false

	case CMP, CP:
		sub(R, fetch(), 0, NZVC)

	case COM:
		store(addr, ^int(fetch()), NZ)
		self.V = false
		self.C = true

	case DAA:
		var ahi = (self.A >> 4) & 0xF
		var alo = self.A & 0xF
		if (self.C || ahi > 9 || (ahi > 8 && alo > 9)) {
			self.A += 0x60
			self.C = true
		}
		if self.H || alo > 9 {
			self.A += 6
		}
		self.A &= 0xFF
		tst(NZ, int(self.A))

	case DEC:
		store(addr, int(sub(fetch(), 1, 0, NZV)), noflags)

	case DE:
		f := noflags
		if register == X { f = Z }
		store(addr, int(sub(fetch(), 1, 0, f)), noflags)

	case EOR:
		store(Reg(register), int(R ^ fetch()), NZV)

	case INC:
		store(addr, int(add(fetch(), 1, 0, NZV)), noflags)

	case IN:
		f := noflags
		if register == X { f = Z }
		store(addr, int(add(fetch(), 1, 0, f)), noflags)

	case JMP:
		self.PC = addr.addr()

	case JSR:
		self.push16(self.PC, S)
		self.PC = addr.addr()

	case LDA, LD:
		store(Reg(register), int(fetch()), NZ)
		self.V = false

	case LSR:
		var v = fetch()
		store(addr, int(0x7F & (v >> 1)), NZ)
		self.C = 1 == v & 1
		self.V = self.N != self.Z

	case NEG:
		var n = -int(fetch())
		store(addr, n, NZ)
		assert(width == 8) // seems to be the assumption below
		self.V = byte(n) == 0x80
		self.C = !self.Z

	case NOP:


	case ORA:
		store(Reg(register), int(R | fetch()), NZ)
		self.V = false

	case PSH:
		assert(width == 8)
		self.push8(byte(R), S)

	case PUL:
		var s = self.pull8(S)
		self.set_reg8(register, s)

	case ROL:
		store(addr, int((byte(fetch()) << 1) | bit(self.C)), NZC)
		self.V = self.N != self.C

	case ROR:
		var v = fetch()
		var msb = bit(self.C) << 7
		store(addr, int(msb | (byte(v) >> 1)), NZ)
		self.C = 1 == v & 1
		self.V = self.N != self.C

	case RTI:
		var cc = self.pull8(S)
		self.set_CC(cc)
		self.B = self.pull8(S)
		self.A = self.pull8(S)
		self.X = self.pull16(S)
		self.PC = self.pull16(S)

	case RTS:
		self.PC = self.pull16(S)

	case SBA:
		self.A = self.sub8(self.A, self.B, 0, NZVC)

	case SBC:
		store(Reg(register), int(sub(R, fetch(), bit16(self.C), NZVC)), noflags)

	case SEC:
		self.C = true

	case SEI:
		self.I = true

	case SEV:
		self.V = true

	case STA, ST:
		store(addr, int(R), NZ)
		self.V = false

	case SUB:
		store(Reg(register), int(sub(R, fetch(), 0, NZVC)), noflags)

	case SWI:
		self.push16(self.PC, S)
		self.push16(self.X, S)
		self.push8(self.A, S)
		self.push8(self.B, S)
		var cc = self.CC()
		self.push8(cc, S)
		if (mnemonic == SWI) { // ?
			self.I = true
			self.F = true
		}
		self.PC = self.fetch16(0xFFFA, false)

	case TAB:
		self.B = self.A
		tst(NZ, int(self.B))
		self.V = false

	case TBA:
		self.A = self.B
		tst(NZ, int(self.A))
		self.V = false

	case TAP:
		self.set_CC(self.A)

	case TPA:
		self.A = self.CC()

	case TST:
		self.V = false
		self.C = false
		tst(NZ, int(fetch()))

	case TSX:
		self.X = self.S

	case TXS:
		self.S = self.X

	case WAI:
		self.push16(self.PC, S)
		self.push16(self.X, S)
		self.push8(self.A, S)
		self.push8(self.B, S)
		var cc = self.CC()
		self.push8(cc, S)
		self.WAIT = true
		return false

	default:
		self.error("Invalid mnemonic " + Mnemonic(mnemonic))
		return false
	}

	// If in a tight busy loop, we can save ourselves some cycles.
	// There"s no single instruction that changes anything and loops,
	// so we"re just waiting for an interrupt.
	if self.PC == opPC {
		self.WAIT = true
	}

	return true
}


func (self *Cpu) step6809() bool {
	if (self.RESET) {
		self.Reset()
		self.elapse(1)
		return false
	}
	if (self.NMI) {
		self.sInitialized = self.sInitialized || self.S != UNINITIALIZED
		if self.sInitialized {
			self.nmi6809()
			self.elapse(1)
			return false
		} else if (self.SYNC) {
			self.SYNC = false
			self.WAIT = false
		}
	}
	if (self.FIRQ) {
		if !self.F {
			self.firq()
			self.elapse(1)
			return false
		} else if (self.SYNC) {
			self.SYNC = false
			self.WAIT = false
		}
	}
	if (self.IRQ) {
		if !self.I {
			self.irq6809()
			self.elapse(1)
			return false
		} else if self.SYNC {
			self.SYNC = false
			self.WAIT = false
		}
	}

	if (self.WAIT || self.SYNC) {
		self.elapse(1)
		return false
	}

	var opcode = uint16(self.nextByte())
	if opcode == 0x10 || opcode == 0x11 {
		opcode = opcode * 256 + uint16(self.nextByte())
	}
	mnemonic, register, mode, timing, _, legality, invalid := Inst6809(opcode)
	if invalid {
		self.error(fmt.Sprintf("Invalid opcode %02X", opcode))
	}

	var autoincrement = 0

	var width = register.bits()

	if legality == FALLBACK { timing += 1 }

	self.elapse(timing)

	var addr Dest
	switch mode {
		case EXTENDED:
			addr = Address(self.nextWord())

		case INHERENT:
			addr = Reg(register)

		case RELATIVE:
			var r = int8(self.nextByte())
			addr = Address(self.PC + uint16(r))

		case LRELATIVE:
			var r = int16(self.nextWord())
			addr = Address(self.PC + uint16(r))

		case DIRECT:
			addr = Address(uint16(self.DP) * 256 + uint16(self.nextByte()))

		case REGISTER:
			var TFR_REGS = []Register{D, X, Y, U, S, PC, o, o,
									  A, B, CC, DP}
			var postbyte = self.nextByte()
			addr = RegPair(TFR_REGS[postbyte & 0xF], TFR_REGS[postbyte >> 4])

		case IMMEDIATE:
			addr = Address(self.PC)
			self.PC++
			if width == 16 { self.PC++ }

		case INDEXED:
			var postbyte = self.nextByte()
			var subregister = []Register{X,Y,U,S}[(postbyte>>5) & 0x3]
			var indirect = ((postbyte & 0x10) != 0)
			var submode = postbyte & 0xF
			var address uint16
			if postbyte == 0x9F {
				// Extended indirect
				self.elapse(5)
				var ptr = self.nextWord()
				address = self.fetch16(ptr, false)
			} else if ((postbyte & 0x80) == 0) {
				// 5-bit offset from register contained in postbyte
				self.elapse(1)
				isubmode := int(submode)
				if indirect { isubmode -= 0x10 }
				address = uint16(int(self.reg(subregister)) + isubmode)
			} else {
				address = self.reg(subregister)
				var offset int
				if submode == 0x4 {
					// No offset from register
					offset = 0
				} else if submode == 0x8 {
					// 8-bit offset from register
					self.elapse(1)
					offset = s8(self.nextByte())
				} else if submode == 0x9 {
					// 16-bit offset from register
					self.elapse(4)
					offset = s16(self.nextWord())
				} else if submode == 0x6 {
					// A-register offset from register
					self.elapse(1)
					offset = int(self.A)
				} else if submode == 0x5 {
					// B-register offset from register
					self.elapse(1)
					offset = int(self.B)
				} else if submode == 0xB {
					// D-register offset from register
					self.elapse(4)
					offset = int(self.D())
				} else if submode == 0 {
					// Post-increment by 1
					self.elapse(2)
					autoincrement = 1
					offset = 0
				} else if submode == 1 {
					// Post-increment by 2
					self.elapse(3)
					autoincrement = 2
					offset = 0
				} else if submode == 2 {
					// Pre-decrement by 1
					self.elapse(2)
					autoincrement = -1
					offset = -1	// it"s a pre-decrement
				} else if submode == 3 {
					// Pre-decrement by 2
					self.elapse(3)
					autoincrement = -2
					offset = -2	// it"s a pre-decrement
				} else if submode == 0xC {
					// 8-bit offset from PC
					self.elapse(1)
					address = 0
					offset = s8(self.nextByte())
				} else if submode == 0xD {
					// 16-bit offset from PC
					self.elapse(5)
					address = 0
					s16(self.nextWord())
				} else {
					self.error(fmt.Sprintf("Invalid indexing submode %02X in postbyte %02X", uint(submode), uint(postbyte)))
					address = 0
				}

				address += uint16(offset)

				if indirect {
					self.elapse(3)
					address = self.fetch16(address, false)
				}
			}

			if autoincrement != 0 {
				var sr = self.reg(subregister)
				self.set_reg(subregister, sr + uint16(autoincrement))
			}

			addr = Address(address)

		default: panic("Impossible")
	}

	store := func(a Dest, v int, f FlagSet) { if width == 16 { self.store16(a,v,f) } else { self.store8(a,v,f) } }
	add := func(a uint16, b uint16, c uint16, f FlagSet) uint16 { if width == 16 { return self.add16(a,b,c,f) } else { return uint16(self.add8(byte(a),byte(b),byte(c),f)) } }
	sub := func(a uint16, b uint16, c uint16, f FlagSet) uint16 { if width == 16 { return self.sub16(a,b,c,f) } else { return uint16(self.sub8(byte(a),byte(b),byte(c),f)) } }
	tst := func(f FlagSet, a int) { if width == 16 { self.tst16(f,a) } else { self.tst8(f,a) } }

	var R uint16
	if register != o { R = self.reg(register) }

	var fetch func () uint16;
	fetch = func () uint16 {  // call only once!
		fetch = func () uint16 { panic("Call only once") }
		if addr.isReg() {
			return R
		} else if width == 16 {
			return self.fetch16(addr.addr(), false)
		} else {
			return uint16(self.fetch8(addr.addr(), false, false))
		}
	}

	switch mnemonic {
	case ABX:
		self.X = self.X + uint16(self.B)

	case ADC:
		var v = fetch()
		self.tstH(R, v, bit(self.C))
		store(Reg(register), int(add(R, v, bit16(self.C), NZVC)), noflags)

	case ADD:
		var v = fetch()
		self.tstH(R, v, 0)
		store(Reg(register), int(add(R, v, 0, NZVC)), noflags)

	case AND:
		var f = NZV
		if register == CC { f = noflags }
		store(Reg(register), int(R & fetch()), f)

	case ASL:
		store(addr, int(fetch()) << 1, NZC)
		self.V = self.N != self.C

	case ASR:
		var v = fetch()
		store(addr, int(v >> 1), NZ)
		self.C = 1 == v & 1

	case BCC, LBCC:
		if !self.C { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BCS, LBCS:
		if self.C { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BEQ, LBEQ:
		if self.Z { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BGE, LBGE:
		if self.N == self.V { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BGT, LBGT:
		if self.N == self.V && !self.Z { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BHI, LBHI:
		if !self.Z && !self.C { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BLE, LBLE:
		if self.N != self.V || self.Z { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BLS, LBLS:
		if self.Z || self.C { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BLT, LBLT:
		if self.N != self.V { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BMI, LBMI:
		if self.N { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BNE, LBNE:
		if !self.Z { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BPL, LBPL:
		if !self.N { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BRA, LBRA:
		self.PC = addr.addr()
	case BRN, LBRN:
		// never!
	case BSR, LBSR:
		self.push16(self.PC, S)
		self.PC = addr.addr()
	case BVC, LBVC:
		if !self.V { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }
	case BVS, LBVS:
		if self.V { self.PC = addr.addr(); if mode == LRELATIVE {self.elapse(1)} }

	case BIT:
		tst(NZV, int(R & fetch()))

	case CLR:
		store(addr, 0, NZVC)

	case CMP:
		sub(R, fetch(), 0, NZVC)

	case COM:
		store(addr, ^int(fetch()), NZV)
		self.C = true

	case CWAI:
		var cc = self.CC() & self.fetch8(addr.addr(), false, false)
		self.set_CC(byte(cc))
		self.E = true
		self.push16(self.PC, S)
		self.push16(self.U, S)
		self.push16(self.Y, S)
		self.push16(self.X, S)
		self.push8(self.DP, S)
		self.push8(self.B, S)
		self.push8(self.A, S)
		self.push8(cc, S)
		self.WAIT = true

	case DAA:
		var ahi = (self.A >> 4) & 0xF
		var alo = self.A & 0xF
		if (self.C || ahi > 9 || (ahi > 8 && alo > 9)) {
			self.A += 0x60
			self.C = true
		}
		if (self.H || alo > 9) {
			self.A += 6
		}
		self.A &= 0xFF
		tst(NZ, int(self.A))

	case DEC:
		store(addr, int(sub(fetch(), 1, 0, NZV)), noflags)

	case EOR:
		store(Reg(register), int(R ^ fetch()), NZV)

	case EXG:
		r1, r2 := addr.asRegPair()
		regOrNeg := func(r Register) uint16 {
			if r == o { return 0xFFFF }
			return self.reg(r)
		}
		v1, v2 := regOrNeg(r1), regOrNeg(r2)
		if r1 != o { self.set_reg(r1, v2) }
		if r2 != o { self.set_reg(r2, v1) }

	case INC:
		store(addr, int(add(fetch(), 1, 0, NZV)), noflags)

	case JMP:
		self.PC = addr.addr()

	case JSR:
		self.push16(self.PC, S)
		self.PC = addr.addr()

	case LD:
		store(Reg(register), int(fetch()), NZV)

	case LEA:
		store(Reg(register), int(addr.addr()), noflags)
		if register == X || register == Y {
			tst(Z, int(addr.addr()))
		}

	case LSR:
		var v = fetch()
		store(addr, int(v >> 1), NZ)
		self.C = 1 == v & 1

	case MUL:
		self.set_D(uint16(self.A) * uint16(self.B))
		self.Z = self.D() == 0
		self.C = 1 == 1 & (self.B >> 7)

	case NEG:
		var v = fetch()
		self.C = v != 0
		store(addr, int(sub(0, v, 0, NZV)), noflags)

	case NOP:


	case HCF:
		self.error("Halting and catching fire as instructed.")

	case OR:
		var f = NZV
		if register == CC { f = noflags }
		store(Reg(register), int(R | fetch()), f)

	case PSHS, PSHU:
		var stack = S
		if mnemonic == PSHU { stack = U }
		var mask = fetch()
		if mask & 0x80 != 0 { self.elapse(2); self.push16(self.PC, stack) }
		if mask & 0x40 != 0 { other := self.S; if stack == S { other = self.U }
							  self.elapse(2); self.push16(other,  stack) }
		if mask & 0x20 != 0 { self.elapse(2); self.push16(self.Y, stack) }
		if mask & 0x10 != 0 { self.elapse(2); self.push16(self.X, stack) }
		if mask & 0x08 != 0 { self.elapse(1); self.push8(self.DP, stack) }
		if mask & 0x04 != 0 { self.elapse(1); self.push8(self.B, stack) }
		if mask & 0x02 != 0 { self.elapse(1); self.push8(self.A, stack) }
		if mask & 0x01 != 0 { var cc = self.CC()
							  self.elapse(1); self.push8(cc, stack) }

	case PULS, PULU:
		var stack = S
		if mnemonic == PULU { stack = U }
		var mask = fetch()
		if mask & 0x01 != 0 { var self_pull8_stack_	 = self.pull8(stack)
							  self.elapse(1); self.set_CC(self_pull8_stack_) }
		if mask & 0x02 != 0 { self.elapse(1); self.A	= self.pull8(stack) }
		if mask & 0x04 != 0 { self.elapse(1); self.B	= self.pull8(stack) }
		if mask & 0x08 != 0 { self.elapse(1); self.DP   = self.pull8(stack) }
		if mask & 0x10 != 0 { self.elapse(2); self.X	= self.pull16(stack) }
		if mask & 0x20 != 0 { self.elapse(2); self.Y	= self.pull16(stack) }
		if mask & 0x40 != 0 { self.elapse(2); var other = self.pull16(stack)
							  if stack == S { self.U = other } else { self.S = other } }
		if mask & 0x80 != 0 { self.elapse(2); self.PC = self.pull16(stack) }

	case ROL:
		store(addr, int((fetch() << 1) | bit16(self.C)), NZVC)

	case ROR:
		var v = byte(fetch())
		var msb = bit(self.C) << 7
		self.C = 1 == v & 1
		store(addr, int(msb | (v >> 1)), NZ)

	case RTI:
		var cc = self.pull8(S)
		self.set_CC(cc)
		if (self.E) {
			self.A = self.pull8(S)
			self.B = self.pull8(S)
			self.DP = self.pull8(S)
			self.X = self.pull16(S)
			self.Y = self.pull16(S)
			self.U = self.pull16(S)
			self.elapse(9)
		}
		self.PC = self.pull16(S)

	case RTS:
		self.PC = self.pull16(S)

	case SBC:
		store(Reg(register), int(sub(R, fetch(), bit16(self.C), NZVC)), noflags)

	case SEX:
		self.tst8(NZ, int(self.B))
		self.set_D(uint16(int8(self.B)))

	case ST:
		store(addr, int(R), NZV)

	case SUB:
		store(Reg(register), int(sub(R, fetch(), 0, NZVC)), noflags)

	case SWI, SWI2, SWI3, RESET:
		self.E = true
		self.push16(self.PC, S)
		self.push16(self.U, S)
		self.push16(self.Y, S)
		self.push16(self.X, S)
		self.push8(self.DP, S)
		self.push8(self.B, S)
		self.push8(self.A, S)
		var cc = self.CC()
		self.push8(cc, S)
		if mnemonic == SWI {
			self.I = true
			self.F = true
		}
		self.PC = self.fetch16(map[int]uint16{
			SWI  : 0xFFFA,
			SWI2 : 0xFFF4,
			SWI3 : 0xFFF2,
			RESET: 0xFFFE}[mnemonic], false)

	case SYNC:
		self.SYNC = true

	case TFR:
		r1, r2 := addr.asRegPair()
		if r1 != o {
			if r2 == o {
				self.set_reg(r1, 0xFFFF)
			} else {
				self.set_reg(r1, self.reg(r2))
			}
		}

	case TST:
		tst(NZV, int(fetch()))

	default:
		self.error(fmt.Sprintf("Unimplemented opcode %02X / %s", uint(opcode), mnemonic))
	}

	return true
}

func (self *Cpu) ResetTo(addr uint16) {
	self.FIRQ = false
	self.IRQ = false
	self.NMI = false
	self.RESET = false
	self.S = 0  // TODO: should this disable NMIs?
	self.WAIT = false
	self.SYNC = false
	self.PC = addr
}

func (self *Cpu) Reset() {
	self.ResetTo(self.fetch16(0xFFFE, false))
}

func (self *Cpu) nmi6800() {
	self.NMI = false
	self.WAIT = false
	self.push16(self.PC, S)
	self.push16(self.X, S)
	self.push8(self.A, S)
	self.push8(self.B, S)
	var cc = self.CC()
	self.push8(cc, S)
	self.I = true
	self.PC = self.fetch16(0xFFFC, false)
}

func (self *Cpu) nmi6809() {
	self.NMI = false
	self.WAIT = false
	self.SYNC = false
	addr := self.fetch16(0xFFFC, false)
	self.E = true
	self.push16(self.PC, S)
	self.push16(self.U, S)
	self.push16(self.Y, S)
	self.push16(self.X, S)
	self.push8(self.DP, S)
	self.push8(self.B, S)
	self.push8(self.A, S)
	var cc = self.CC()
	self.push8(cc, S)
	self.F = true
	self.I = true
	self.PC = addr
}

func (self *Cpu) firq() {
	self.FIRQ = false
	self.WAIT = false
	self.SYNC = false
	self.E = false
	self.push16(self.PC, S)
	var cc = self.CC()
	self.push8(cc, S)
	self.F = true
	self.I = true
	self.PC = self.fetch16(0xFFF6, false)
}

func (self *Cpu) irq6800() {
	self.IRQ = false
	self.WAIT = false
	self.push16(self.PC, S)
	self.push16(self.X, S)
	self.push8(self.A, S)
	self.push8(self.B, S)
	var cc = self.CC()
	self.push8(cc, S)
	self.I = true
	self.PC = self.fetch16(0xFFF8, false)
}

func (self *Cpu) irq6809() {
	self.IRQ = false
	self.WAIT = false
	self.SYNC = false
	self.E = true
	self.push16(self.PC, S)
	self.push16(self.U, S)
	self.push16(self.Y, S)
	self.push16(self.X, S)
	self.push8(self.DP, S)
	self.push8(self.B, S)
	self.push8(self.A, S)
	var cc = self.CC()
	self.push8(cc, S)
	self.I = true
	self.PC = self.fetch16(0xFFF8, false)
}

func (self *Cpu) showram(andzero bool) {
	println("RAM:")
	for i := 0; i < len(self.Ram); i++ {
		var v = self.Ram[i]
		if andzero || v != 0 {
			fmt.Printf("	%04X: %02X %4d", i, v, int8(v))
		}
	}
}

func (self *Cpu) vectors() (result map[string]uint16) {
	result["reset"] = self.fetch16(0xFFFE, false)
	result["nmi"] = self.fetch16(0xFFFC, false)
	result["swi"] = self.fetch16(0xFFFA, false)
	result["irq"] = self.fetch16(0xFFF8, false)
	if self.partNumber == MC6809 {
		result["firq"] = self.fetch16(0xFFF6, false)
		result["swi2"] = self.fetch16(0xFFF4, false)
		result["swi3"] = self.fetch16(0xFFF2, false)
	}
	return
}

func (self *Cpu) run(limit float64) {
	var initial = self.Clock()
	for !self.WAIT && !self.SYNC && self.Clock() - initial < limit {
		var pc = self.PC
		if self.Verbose {
			self.Trace()
		}
		if !self.Step() {
			break
		}
		if pc == self.PC {
			self.error("INFINITE LOOP")
			break
		}
	}
}

func (self *Cpu) elapse(cycles int) {
	self.CLOCK += cycles
	if (self.CLOCK > 1000000) {
		self.MCLOCK += 1
		self.CLOCK -= 1000000
	}
}

func (self *Cpu) Clock() float64 { return (float64(self.MCLOCK) + float64(self.CLOCK) / 1000000.0) / self.clockRate }

func (self *Cpu) HookWrite(addr uint16, callback func(byte, uint16)) {
	self.writeHooks[addr] = callback
}

func (self *Cpu) HookRead(addr uint16, callback func(uint16)) {
	self.readHooks[addr] = callback
}

func (self *Cpu) ConnectPia(pia *Pia6821, addr uint16) {
	pia.mpu = self
	pia.address = addr
	self.pias = append(self.pias, pia)
}

func (self *Cpu) Step() bool {
	if self.partNumber == MC6809 {
		return self.step6809()
	} else {
		return self.step6800()
	}
}

func (self *Cpu) Disassemble(pc uint16, withState bool) (asm string, nbytes uint16) {
	if self.partNumber == MC6809 {
		return self.disassemble6809(pc, withState)
	} else {
		return self.disassemble6800(pc, withState)
	}
}

func (self *Cpu) Trace() {
	var asm, _ = self.Disassemble(self.PC, true)
	fmt.Printf("%s\n", asm)
}

func (self *Cpu) dump(start uint16, len uint16) {
	r := ""
	for i := uint16(0); i < len; i++ {
		r += fmt.Sprintf("%02X ", self.Fetch(start + i))
		if i % 16 == 15 { r += "\n" }
	}
	fmt.Println(r)
}


type Channel struct {
	CR byte
	P byte
	DDR byte
	OUTPUT func(byte)
}

type Pia6821 struct {
	channel [2]Channel
	address uint16
	mpu *Cpu
}


func newPia6821() Pia6821 {
	return Pia6821 {
		// channel: [Channel { CR:0, P:0, DDR:0, OUTPUT: nothing },
		//		   Channel { CR:0, P:0, DDR:0, OUTPUT: nothing }],
		// address: 0,
		// mpu: ptr::mut_null(),
	}
}

func (self *Pia6821) Reset() {
	self.channel[0].CR = 0
	self.channel[1].CR = 0
	self.channel[0].P = 0
	self.channel[1].P = 0
	self.channel[0].DDR = 0
	self.channel[1].DDR = 0
}

func (self *Pia6821) interruptEnabledOnIndex(index int) bool {
	var cr = self.channel[index].CR
	return cr & 0x1 != 0 || (!(cr & 0x20 != 0) && (cr & 0x08 != 0))
}

func (self *Pia6821) ConnectPeripheralOutput(index int, callback func(byte)) {
	self.channel[index].OUTPUT = callback
}

func (self *Pia6821) PeripheralInput(index int, value byte, mask byte) {
	//if undef(mask) { mask = ~self.channel[index].DDR }
	mask &= ^self.channel[index].DDR
	self.channel[index].P = (value & mask) | (self.channel[index].P & ^mask)
	if (self.interruptEnabledOnIndex(index)) {
		self.mpu.IRQ = true
	}
}

func (self *Pia6821) write(offset uint16, value byte) {
	var tipe = offset & 1
	var index = offset >> 1
	if (tipe == 0) {
		if self.channel[index].CR & 0x4 != 0 {
			self.channel[index].P = (value & self.channel[index].DDR) |
				(self.channel[index].P & ^self.channel[index].DDR)
			outp := self.channel[index].OUTPUT
			if outp != nil {
				outp(self.channel[index].P)
			}
		} else {
			self.channel[index].DDR = value
		}
	} else {
		self.channel[index].CR = value
	}
}

func (self *Pia6821) read(offset uint16) byte {
	var tipe = offset & 1
	var index = int(offset >> 1)
	if (tipe == 0) {
		if (self.channel[index].CR & 0x4 != 0) {
			return self.channel[index].P
		} else {
			return self.channel[index].DDR
		}
	} else {
		var result = self.channel[index].CR
		if (self.channel[index].CR & 0xC0 != 0) {
			if (self.interruptEnabledOnIndex(index)) {
				self.mpu.IRQ = false
			}
			self.channel[index].CR &= 0x3F	// clear interrupt status bits
		}
		return result
	}
}

func (self *Pia6821) toString() string {
	ddr := fmt.Sprintf("%02X", self.channel[1].DDR)
	switch self.channel[1].DDR {
	case 0:    ddr = "ii"
	case 0xFF: ddr = "oo"
	}
	return fmt.Sprintf("%4X: A[%s] %02X %02X / B%s %02X %02X",
				uint(self.address),
				uint(self.channel[0].P),
				uint(self.channel[0].CR),
				ddr,
				uint(self.channel[1].P),
				uint(self.channel[1].CR))
}

func test() {
	main := CpuNew(MC6809, 1.0)
	main.CreateRam(0, 0xD000)
	testbin, _ := ioutil.ReadFile("build/test.bin")
	main.LoadRom(0xF000, testbin)
	main.ResetTo(0xF000)
	fmt.Printf("%04X: %s\n", main.PC, main.state())
	main.dump(0xF000, 134)
	main.Verbose = true
	main.run(0.0001)
	fmt.Printf("%04X: %s\n", main.PC, main.state())
	// main.showram()
}
