package emu

const (
	ABA = iota + 1
	ABX
	ADC
	ADD
	AND
	ASL
	ASR
	BCC
	BCS
	BEQ
	BGE
	BGT
	BHI
	BIT
	BLE
	BLS
	BLT
	BMI
	BNE
	BPL
	BRA
	BRN
	BSR
	BVC
	BVS
	CBA
	CLC
	CLI
	CLR
	CLV
	CMP
	COM
	CP
	CWAI
	DAA
	DE
	DEC
	DROP
	EOR
	EXG
	HCF
	IN
	INC
	JMP
	JSR
	LBCC
	LBCS
	LBEQ
	LBGE
	LBGT
	LBHI
	LBLE
	LBLS
	LBLT
	LBMI
	LBNE
	LBPL
	LBRA
	LBRN
	LBSR
	LBVC
	LBVS
	LD
	LDA
	LEA
	LSR
	MUL
	NECO
	NEG
	NOP
	OR
	ORA
	PSH
	PSHS
	PSHU
	PUL
	PULS
	PULU
	RESET
	ROL
	ROR
	RTI
	RTS
	SBA
	SBC
	SCC
	SEC
	SEI
	SEV
	SEX
	ST
	STA
	SUB
	SWI
	SWI2
	SWI3
	SYNC
	TAB
	TAP
	TBA
	TFR
	TPA
	TRACE
	TST
	TSX
	TXS
	WAI
	WTF
)

func Mnemonic(instruction int) string {
	switch (instruction) {
		case ABA: return "ABA"
		case ABX: return "ABX"
		case ADC: return "ADC"
		case ADD: return "ADD"
		case AND: return "AND"
		case ASL: return "ASL"
		case ASR: return "ASR"
		case BCC: return "BCC"
		case BCS: return "BCS"
		case BEQ: return "BEQ"
		case BGE: return "BGE"
		case BGT: return "BGT"
		case BHI: return "BHI"
		case BIT: return "BIT"
		case BLE: return "BLE"
		case BLS: return "BLS"
		case BLT: return "BLT"
		case BMI: return "BMI"
		case BNE: return "BNE"
		case BPL: return "BPL"
		case BRA: return "BRA"
		case BRN: return "BRN"
		case BSR: return "BSR"
		case BVC: return "BVC"
		case BVS: return "BVS"
		case CBA: return "CBA"
		case CLC: return "CLC"
		case CLI: return "CLI"
		case CLR: return "CLR"
		case CLV: return "CLV"
		case CMP: return "CMP"
		case COM: return "COM"
		case CP: return "CP"
		case CWAI: return "CWAI"
		case DAA: return "DAA"
		case DE: return "DE"
		case DEC: return "DEC"
		case DROP: return "DROP"
		case EOR: return "EOR"
		case EXG: return "EXG"
		case HCF: return "HCF"
		case IN: return "IN"
		case INC: return "INC"
		case JMP: return "JMP"
		case JSR: return "JSR"
		case LBCC: return "LBCC"
		case LBCS: return "LBCS"
		case LBEQ: return "LBEQ"
		case LBGE: return "LBGE"
		case LBGT: return "LBGT"
		case LBHI: return "LBHI"
		case LBLE: return "LBLE"
		case LBLS: return "LBLS"
		case LBLT: return "LBLT"
		case LBMI: return "LBMI"
		case LBNE: return "LBNE"
		case LBPL: return "LBPL"
		case LBRA: return "LBRA"
		case LBRN: return "LBRN"
		case LBSR: return "LBSR"
		case LBVC: return "LBVC"
		case LBVS: return "LBVS"
		case LD: return "LD"
		case LDA: return "LDA"
		case LEA: return "LEA"
		case LSR: return "LSR"
		case MUL: return "MUL"
		case NECO: return "NECO"
		case NEG: return "NEG"
		case NOP: return "NOP"
		case OR: return "OR"
		case ORA: return "ORA"
		case PSH: return "PSH"
		case PSHS: return "PSHS"
		case PSHU: return "PSHU"
		case PUL: return "PUL"
		case PULS: return "PULS"
		case PULU: return "PULU"
		case RESET: return "RESET"
		case ROL: return "ROL"
		case ROR: return "ROR"
		case RTI: return "RTI"
		case RTS: return "RTS"
		case SBA: return "SBA"
		case SBC: return "SBC"
		case SCC: return "SCC"
		case SEC: return "SEC"
		case SEI: return "SEI"
		case SEV: return "SEV"
		case SEX: return "SEX"
		case ST: return "ST"
		case STA: return "STA"
		case SUB: return "SUB"
		case SWI: return "SWI"
		case SWI2: return "SWI2"
		case SWI3: return "SWI3"
		case SYNC: return "SYNC"
		case TAB: return "TAB"
		case TAP: return "TAP"
		case TBA: return "TBA"
		case TFR: return "TFR"
		case TPA: return "TPA"
		case TRACE: return "TRACE"
		case TST: return "TST"
		case TSX: return "TSX"
		case TXS: return "TXS"
		case WAI: return "WAI"
		case WTF: return "WTF"
		default: return "???"
	}
}
