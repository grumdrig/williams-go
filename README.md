Williams-Go
===========

Emulator for Willams arcade machine hardware (Defender, Stargate, Robotron, Joust).

This is a port (via Rust!) of [my JS version](https://bitbucket.org/grumdrig/williams)


Caveats
-------

* Not quite 100% totally working yet!

* You'll need to find ROMs somewhere, preferably legally


Go!
---

* `% go build williams`

* `% ./williams -f -a stargate`


Things I Find Annoying About Go
-------------------------------

* No ternary operator, esp. since `if` clauses don't return a value

* `for i := 0; i < 10; ++i` here in two-thousand-whatever. Verbose and redundant (and
  therefore prone to errors). (`for i := range 10` should work.)

* Wacky array and map literal decl syntax

* No generics

* Global sort of build system seems a little weird, maybe


Things I Find Awesome About Go (esp. as compared to Rust)
---------------------------------------------------------

* cgo is charming

