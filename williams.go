package main

import (
	"math"
	"fmt"
	"runtime"
	"flag"
	"os"
	"log"
	"path"
	"strings"
	"io/ioutil"
	"archive/zip"
	"container/list"
	"runtime/pprof"
	"williams/emu"
	"williams/sdl"
)


func blit(mpu *emu.Cpu, start byte, mask byte, source uint16, dest uint16, width byte, height byte) {
	// There"s a bug in the blitter that xors bit 2 of the height and
	// width. It's fixed in later hardware, but not in any that I
	// currently care about.
	m_blitter_xor := byte(0x4)
	width ^= m_blitter_xor
	height ^= m_blitter_xor
	var sxstride uint16 = 1
	var dxstride uint16 = 1
	if start & 1 != 0 { sxstride = 256 }
	if start & 2 != 0 { dxstride = 256 }
	var transparency = start & 0x8 != 0
	var solid = start & 0x10 != 0
	var ror = start & 0x20 != 0
	var even = start & 0x40 == 0
	var odd = start & 0x80 == 0

	for _y := byte(0); _y < height; _y++ {
		var s = source
		var d = dest
		var oldc byte = 0
		if ror { oldc = mpu.Fetch((s + sxstride * (uint16(width) - 1)) & 0xFFFF) }
		for _x := byte(0); _x < width; _x++ {
			var c = mpu.Fetch(s)
			if (ror) {
				var newc = 0xFF & ((oldc << 4) | (c >> 4))
				oldc = c
				c = newc
			}
			if solid {
				if transparency {
					hi, lo := c & 0xF0 != 0, c & 0x0F != 0
					c = 0
					if hi {
						c = mask & 0xF0
					}
					if lo {
						c |= mask & 0x0F
					}
				} else {
					c = mask
				}
			}
			if !even || !odd || transparency {
				var oc = mpu.FetchRam(d)
				if !even || (transparency && c & 0x0F == 0) {
					c = (c & 0xF0) | (oc & 0x0F)
				}
				if !odd || (transparency && c & 0xF0 == 0) {
					c = (c & 0x0F) | (oc & 0xF0)
				}
			}
			mpu.Store(d, c)
			s = s + sxstride
			d = d + dxstride
		}

		if start & 1 != 0 {
			source = (source & 0xff00) | ((source + 1) & 0xff)
		} else {
			source += uint16(width)
		}

		if start & 2 != 0 {
			dest = (dest & 0xff00) | ((dest + 1) & 0xff)
		} else {
			dest += uint16(width)
		}
	}
}


type Sample struct {
	time float64
	value byte
}

type Audio struct {
	deviceid uint32
	samplesSeen uint
	samples list.List
	current byte
}

const SAMPLE_PERIOD = 1.0 / 8000.0  // there I go making assumptions

func (self *Audio) clock() float64 {
	return float64(self.samplesSeen) * SAMPLE_PERIOD
}

func (self *Audio) addSample(time float64, value byte) {
	if self.deviceid > 0 {
		sdl.LockAudioDevice(self.deviceid)
		self.samples.PushBack(Sample { time:time, value:value })
		sdl.UnlockAudioDevice(self.deviceid)
	}
	// fmt.Printf("%f %02X\n", time, value)
}

func (self *Audio) play(buffer []byte) {
	if self.samples.Len() > 0 {
		t := self.samples.Front().Value.(Sample).time
		if self.clock() > t {
			// fmt.Printf("WARNING: audio playback is ahead of game by %f sec\n", self.clock() - t)
			self.samplesSeen = uint(t / SAMPLE_PERIOD)
		}
	}
	for i := 0; i < len(buffer); i++ {
		// if i == 0 { fmt.Println("NOW", now, self.samplesSeen) }
		if self.samples.Len() > 0 && self.samples.Front().Value.(Sample).time < self.clock() {
			// s := self.samples.Front().Value.(Sample)
			// fmt.Printf("%f %02X\n", s.time, s.value)
			self.current = self.samples.Remove(self.samples.Front()).(Sample).value
		}
		self.samplesSeen++;
		buffer[i] = self.current
	}
}


const (
	defender   	= "defender"
	defendg   	= "defendg"
	defendw   	= "defendw"
	stargate   	= "stargate"
	robotron   	= "robotron"
	robotryo   	= "robotryo"
	joust   	= "joust"
	joustr   	= "joustr"
	joustwr   	= "joustwr"
	blittest   	= "blittest"
)


type Key struct {
 	pia *emu.Pia6821
 	channel int
 	bit uint
 	description string
 }


type Game struct {
	main *emu.Cpu
	sound *emu.Cpu
	name string
	label string
	vertical_count byte
	video_counter_address uint16
	watchdog uint16
	widgetpia *emu.Pia6821
	rompia *emu.Pia6821
	soundpia *emu.Pia6821
	keymap map[int]Key
	audio Audio
}


func NewGame() *Game {

	var game = Game {
		main:  emu.CpuNew(emu.MC6809, 1.000000),
		sound: emu.CpuNew(emu.MC6808, 0.895000),
		widgetpia: &emu.Pia6821{},
		rompia:    &emu.Pia6821{},
		soundpia:  &emu.Pia6821{},
		keymap: map[int]Key{},
	}

	return &game
}

func (self *Game) bank0000(bank []byte) {
	self.main.HookWrite(0xC900, func (b byte, a uint16) {
		if (b & 1) != 0 {
			self.main.LoadRom(0x0000, bank)
		} else {
			self.main.UnloadRom(0x0000)
		}
		// $("#main .bank").innerHTML = rom ? "ROM" : "DRAM"
	})
}

func (self *Game) bankC000(banks map[byte][]byte) {
	self.main.HookWrite(0xD000, func (b byte, a uint16) {
		// Select ROM bank at C000-CFFF
		// $("#main .bank").innerHTML = b || "I/O"
		self.main.LoadRom(0xC000, banks[b])
	})
}

func (self *Game) loadRomset(romdir string, romset string) {
	if romset != blittest {
		self.main.CreateRam(0, 0xC010)
		self.sound.CreateRam(0, 128)
	}

	read_roms := func (roms []string) []byte {
		r := []byte{}
		for _,s := range roms {
			ss := strings.Split(s, "/")
			var archive, filename string
			if len(ss) == 2 {
				archive, filename = ss[0], ss[1]
			} else {
				archive, filename = "", s
			}
			data, err := ioutil.ReadFile(path.Join(romdir, filename))
			if err != nil {
				if arch, err := zip.OpenReader(path.Join(romdir, archive + ".zip")); err == nil {
					for _, f := range arch.File {
						if f.Name == filename {
							rdr, _ := f.Open()
							data, err = ioutil.ReadAll(rdr)
		 					break
		 				}
		 			}
		 			arch.Close()
		     	}
	     	}
			if len(data) == 0 {
				panic("Unable to load ROM " + s)
			}
			r = append(r, data...)
		}
		return r
	}

	switch romset {
		case "defender":
			self.name = "Defender"
			self.label = "Red"
			self.main.LoadRom(0xD000, read_roms(
				[]string{"defender/defend.1", "defender/defend.4",
				         "defender/defend.2", "defender/defend.3"}))
			self.bankC000(map[byte][]byte{
				 1: read_roms([]string{"defender/defend.9", "defender/defend.12"}),
				 2: read_roms([]string{"defender/defend.8", "defender/defend.11"}),
				 3: read_roms([]string{"defender/defend.7", "defender/defend.10"}),
				 7: read_roms([]string{"defender/defend.6"}),
			})
			self.sound.LoadRom(0xF800, read_roms([]string{"defender/defend.snd"}))

		case "defendg":
			self.name = "Defender"
			self.label = "Green"
			self.main.LoadRom(0xD000, read_roms(
				[]string{"defendg/defeng01.bin", "defendg/defeng04.bin",
						 "defendg/defeng02.bin", "defendg/defeng03.bin"}))
			self.bankC000(map[byte][]byte{
				 1: read_roms([]string{"defendg/defeng09.bin", "defendg/defeng12.bin"}),
				 2: read_roms([]string{"defendg/defeng08.bin", "defendg/defeng11.bin"}),
				 3: read_roms([]string{"defendg/defeng07.bin", "defendg/defeng10.bin"}),
				 7: read_roms([]string{"defendg/defeng06.bin"}),
			})
			self.sound.LoadRom(0xF800, read_roms([]string{"defender/defend.snd"}))

		case "defendw":
			self.name = "Defender"
			self.label = "White"
			self.main.LoadRom(0xD000, read_roms(
				[]string{"defendw/wb01.bin", "defendw/defeng02.bin", "defendw/wb03.bin"}))
			self.bankC000(map[byte][]byte{
				 1: read_roms([]string{"defendw/defeng09.bin", "defendw/defeng12.bin"}),
				 2: read_roms([]string{"defendw/defeng08.bin", "defendw/defeng11.bin"}),
				 3: read_roms([]string{"defendw/defeng07.bin", "defendw/defeng10.bin"}),
				 7: read_roms([]string{"defendw/defeng06.bin"}),
			})
			self.sound.LoadRom(0xF800, read_roms([]string{"defender/defend.snd"}))

		case "stargate":
			self.name = "Stargate"
			self.bank0000(read_roms([]string{"stargate/01", "stargate/02", "stargate/03",
											 "stargate/04", "stargate/05", "stargate/06",
											 "stargate/07", "stargate/08", "stargate/09"}))
			self.main.LoadRom(0xD000, read_roms([]string{"stargate/10", "stargate/11", "stargate/12"}))
			self.sound.LoadRom(0xF800, read_roms([]string{"stargate/sg.snd"}))

		case "robotron":
			self.name = "Robotron"
			self.label = "Solid Blue"
			self.bank0000(read_roms([]string{"robotron/robotron.sb1", "robotron/robotron.sb2", "robotron/robotron.sb3",
											 "robotron/robotron.sb4", "robotron/robotron.sb5", "robotron/robotron.sb6",
											 "robotron/robotron.sb7", "robotron/robotron.sb8", "robotron/robotron.sb9"}))
			self.main.LoadRom(0xD000, read_roms([]string{"robotron/robotron.sba", "robotron/robotron.sbb", "robotron/robotron.sbc"}))
			self.sound.LoadRom(0xF000, read_roms([]string{"robotron/robotron.snd"}))
			// Monkey-patch-out the memory test
			// (but not quite right)
			//self.main.patch(0xF472, 0x12)
			//self.main.patch(0xF473, 0x12)
			//self.main.patch(0xF474, 0x12)

		case "robotryo":
			self.name = "Robotron"
			self.label = "Yellow/Orange"
			self.bank0000(read_roms([]string{"robotron/robotron.sb1", "robotron/robotron.sb2", "robotryo/robotron.yo3",
											 "robotryo/robotron.yo4", "robotryo/robotron.yo5", "robotryo/robotron.yo6",
											 "robotryo/robotron.yo7", "robotron/robotron.sb8", "robotron/robotron.sb9"}))
			self.main.LoadRom(0xD000, read_roms([]string{"robotryo/robotron.yoa", "robotryo/robotron.yob", "robotryo/robotron.yoc"}))
			self.sound.LoadRom(0xF000, read_roms([]string{"robotron/robotron.snd"}))

		case "joust":
			self.name = "Joust"
			self.label = "White/Green"
			self.bank0000(read_roms([]string{"joust/3006-13.1b", "joust/3006-14.2b", "joust/3006-15.3b",
											 "joust/3006-16.4b", "joust/3006-17.5b", "joust/3006-18.6b",
											 "joust/3006-19.7b", "joust/3006-20.8b", "joust/3006-21.9b"}))
			self.main.LoadRom(0xD000, read_roms([]string{"joust/3006-22.10b", "joust/3006-23.11b", "joust/3006-24.12b"}))
			self.sound.LoadRom(0xF000, read_roms([]string{"joust/joust.snd"}))

		case "joustr":
			self.name = "Joust"
			self.label = "Solid Red"
			self.bank0000(read_roms([]string{"joustr/joust.wg1", "joustr/joust.wg2", "joustr/joust.wg3",
											 "joustr/joust.sr4", "joustr/joust.wg5", "joustr/joust.sr6",
											 "joustr/joust.sr7", "joustr/joust.sr8", "joustr/joust.sr9"}))
			self.main.LoadRom(0xD000, read_roms([]string{"joustr/joust.sra", "joustr/joust.srb", "joustr/joust.src"}))
			self.sound.LoadRom(0xF000, read_roms([]string{"joust/joust.snd"}))

		case "joustwr":
			self.name = "Joust"
			self.label = "White/Red"
			self.bank0000(read_roms([]string{"joustwr/joust.wg1", "joustwr/joust.wg2", "joustwr/joust.wg3",
											 "joustwr/joust.wg4", "joustwr/joust.wg5", "joustwr/joust.wg6",
											 "joustwr/joust.wr7", "joustwr/joust.wg8", "joustwr/joust.wg9"}))
			self.main.LoadRom(0xD000, read_roms([]string{"joustwr/joust.wra", "joustwr/joust.wgb", "joustwr/joust.wgc"}))
			self.sound.LoadRom(0xF000, read_roms([]string{"joust/joust.snd"}))

		case "blittest":
			self.name = "Blit Test"
			self.main.LoadRam(0x0000, read_roms([]string{"blittest.bin"}))
			var pal = [...]byte{0x00,0xff,0x07,0x05,0x38,0xc7,0xc0,0x80,0xa4,0xe8,0x14,0x90,0x3f,0x51,0x0a,0x67}
			for i := uint16(0); i < 16; i++ {
				self.main.Store(0xC000 + i, pal[i]) //random())
			}
			// Stub sound
			self.sound.CreateRam(0, 0x10000)
			for i := uint16(0); i < 0xF030; i += 3 {
				self.sound.Store(i, 0x7E)
				self.sound.Storew(i+1, 0xF000)
			}
			self.sound.Storew(0xFFFE, 0xF000)

		default:
			panic("Invalid or unsupported ROM set")
	}

	if self.name == "Defender" {
		// Older, defender hardware configuration
		self.video_counter_address = 0xC800
		self.watchdog = 0xC3F3
		self.main.ConnectPia(self.widgetpia, 0xCC04)
		self.main.ConnectPia(self.rompia, 0xCC00)
		self.main.DefineIO(0xC010, 0xD001 - 0xC010)
	} else {
		// Newer, robotron/joust hardware configuration
		self.video_counter_address = 0xCB00
		self.watchdog = 0xCBFF
		self.main.ConnectPia(self.widgetpia, 0xC804)
		self.main.ConnectPia(self.rompia, 0xC80C)
		var blt = func (startCode byte, addr uint16) {
			mask, source, dest, width, height :=
					 self.main.Fetch(0xCA01),
						self.main.Fetchw(0xCA02),
						self.main.Fetchw(0xCA04),
						self.main.Fetch(0xCA06),
						self.main.Fetch(0xCA07)
			blit(self.main, startCode, mask, source, dest, width, height)
		}
		self.main.HookWrite(0xCA00, blt)
		if self.name != "Blit Test" {
			self.main.DefineIO(0xC010, 0x1000-0x10)
		}
	}
	switch self.name {
	case "Stargate":
		self.keymap['i'] = Key { self.widgetpia, 1, 0, "inviso" }
		self.keymap['t'] = Key { self.rompia, 0, 6, "slam door tilt" }
		fallthrough
	case "Defender":
		self.keymap['l'] = Key {self.widgetpia, 0, 0, "fire"}
		self.keymap['k'] = Key {self.widgetpia, 0, 1, "thrust"}
		self.keymap['b'] = Key {self.widgetpia, 0, 2, "spart bomb"}
		self.keymap['h'] = Key {self.widgetpia, 0, 3, "hyperspace"}
		self.keymap['2'] = Key {self.widgetpia, 0, 4, "2 players"}
		self.keymap['1'] = Key {self.widgetpia, 0, 5, "1 player"}
		self.keymap['d'] = Key {self.widgetpia, 0, 6, "reverse"}
		self.keymap['s'] = Key {self.widgetpia, 0, 7, "down"}
		self.keymap['w'] = Key {self.widgetpia, 1, 0, "up"}
		self.keymap['9'] = Key {self.rompia, 0, 0, "auto up"}
		self.keymap['8'] = Key {self.rompia, 0, 1, "advance"}
		self.keymap['6'] = Key {self.rompia, 0, 2, "right coin"}
		self.keymap['0'] = Key {self.rompia, 0, 3, "high score reset"}
		self.keymap['5'] = Key {self.rompia, 0, 4, "left coin"}
		self.keymap['7'] = Key {self.rompia, 0, 5, "center coin"}

	case "Robotron":
		self.keymap['e'] = Key { self.widgetpia, 0, 0, "move up" }
		self.keymap['d'] = Key { self.widgetpia, 0, 1, "move down" }
		self.keymap['s'] = Key { self.widgetpia, 0, 2, "move left" }
		self.keymap['f'] = Key { self.widgetpia, 0, 3, "move right" }
		self.keymap['1'] = Key { self.widgetpia, 0, 4, "1 player start" }
		self.keymap['2'] = Key { self.widgetpia, 0, 5, "2 player start" }
		self.keymap['i'] = Key { self.widgetpia, 0, 6, "fire up" }
		self.keymap['k'] = Key { self.widgetpia, 0, 7, "fire down" }
		self.keymap['j'] = Key { self.widgetpia, 1, 0, "fire left" }
		self.keymap['l'] = Key { self.widgetpia, 1, 1, "fire right" }
		self.keymap['9'] = Key { self.rompia, 0, 0, "auto up" }
		self.keymap['8'] = Key { self.rompia, 0, 1, "advance" }
		self.keymap['6'] = Key { self.rompia, 0, 2, "right coin" }
		self.keymap['0'] = Key { self.rompia, 0, 3, "high score reset" }
		self.keymap['5'] = Key { self.rompia, 0, 4, "left coin" }
		self.keymap['t'] = Key { self.rompia, 0, 5, "slam door tilt" }

	case "Joust":
		// Player 1 and player 2 are muxed on these somehow
		self.keymap['s'] = Key { self.widgetpia, 0, 0, "move left" }
		self.keymap['f'] = Key { self.widgetpia, 0, 1, "move right" }
		self.keymap[' '] = Key { self.widgetpia, 0, 2, "flap" }
		self.keymap['2'] = Key { self.widgetpia, 0, 4, "2 player start" }
		self.keymap['1'] = Key { self.widgetpia, 0, 5, "1 player start" }
		self.keymap['9'] = Key { self.rompia, 0, 0, "auto up" }
		self.keymap['8'] = Key { self.rompia, 0, 1, "advance" }
		self.keymap['6'] = Key { self.rompia, 0, 2, "right coin" }
		self.keymap['0'] = Key { self.rompia, 0, 3, "high score reset" }
		self.keymap['5'] = Key { self.rompia, 0, 4, "left coin" }
		self.keymap['7'] = Key { self.rompia, 0, 5, "center coin" }
		self.keymap['t'] = Key { self.rompia, 0, 6, "slam door tilt" }
	}

	// Not sure if this is quite right, esp for defender, but starting with advance and
	// auto up pressed gets us into the game
	self.rompia.PeripheralInput(0, 0x3, 0x3)

	self.sound.ConnectPia(self.soundpia, 0x0400)

	self.rompia.ConnectPeripheralOutput(1, func (value byte) {
		self.soundpia.PeripheralInput(1, value, 0xFF)
	})

	self.soundpia.ConnectPeripheralOutput(0, func (value byte) {
		self.audio.addSample(self.sound.Clock(), value)
	})

	self.main.Reset()
	self.sound.Reset()
}


func (self *Game) RunFrame() {
	for {
		if self.sound.Clock() < self.main.Clock() {
			if self.sound.Verbose {
				self.sound.Trace();
			}
			self.sound.Step()
		} else {
			if self.main.Verbose {
				self.main.Trace();
			}
			self.main.Step()

			var vcount = byte(self.main.Clock() * 60.0 * 256.0)
			if self.vertical_count != vcount {
				self.vertical_count = vcount
				self.main.Store(self.video_counter_address, self.vertical_count & 0xFC)

				if self.vertical_count == 240 || self.vertical_count == 0 {
					self.rompia.PeripheralInput(1, 0xFF, 0xFF)
				}
				if self.vertical_count == 0 {
					break 	// That's a vblank, folks
				}
			}
		}
	}
}

func (self *Game) HandleKey(down bool, c int) {
	key,ok := self.keymap[c]
	if ok {
		var mask = byte(1 << key.bit)
		thing := byte(0)
		if down {thing = mask}
		key.pia.PeripheralInput(key.channel, thing, mask)
	}
}

func (self *Game) system_clock() float64 {
	return math.Min(self.main.Clock(), self.sound.Clock())
}

func Color(r byte, g byte, b byte) uint32 {
	return (uint32(r) << 0) | (uint32(g) << 8) | (uint32(b) << 16) | 0xFF000000
}

func color(bbgggrrr byte) uint32 {
	var r = 0xE0 & (bbgggrrr << 5)
	var g = 0xE0 & (bbgggrrr << 2)
	var b = 0xC0 & bbgggrrr
	r |= (r >> 3) | (r >> 6)
	g |= (g >> 3) | (g >> 6)
	b |= (b >> 2) | (b >> 4) | (b >> 6)
	return Color(r, g, b)
}

func Render(ram []byte, pixels []uint32, pitch int) {
	var pal16 [16]uint32
	for i := 0; i < 16; i++ {
		pal16[i] = color(ram[0xC000 + i])
	}
	var put = func (x int, y int, c uint32) {
		var d = x + y * pitch
		pixels[d] = c
	}
	for xy := 0; xy < 256 * 304 / 2; xy++ {
		var y = xy & 0xFF
		var x0 = 2 * (xy >> 8)
		put(x0,	 y, pal16[ram[xy] >> 4])
		put(x0+1, y, pal16[ram[xy] & 0xF])
	}
}



func main() {
	runtime.LockOSThread()

	var game = NewGame()

	romdir := flag.String("d", "./roms", "directory containing [zip archives of] ROM files")
	romset := flag.String("r", "defender", "game rom set to load - one of:\n         defender defendg defendw stargate robotron robotryo\n         joust joustr joustwr blittest")
	flag.BoolVar(&game.main.Verbose, "m", false, "print instruction trace for main CPU")
	flag.BoolVar(&game.sound.Verbose, "s", false, "print instruction trace for sound CPU")
	scale := flag.Int("n", 1, "scale up resolution by multiplier")
	fullscreen := flag.Bool("f", false, "fullscreen")
	enableaudio := flag.Bool("a", false, "enable audio")
	cpuprofile := flag.String("cpuprofile", "", "write cpu profile to file")
	flag.Parse()

	if len(flag.Args()) > 0 {
		flag.Usage()
		os.Exit(-1)
	}

    if *cpuprofile != "" {
        f, err := os.Create(*cpuprofile)
        if err != nil {
            log.Fatal(err)
        }
        pprof.StartCPUProfile(f)
        defer pprof.StopCPUProfile()
    }

	game.loadRomset(*romdir, *romset)

	title := game.name
	if len(game.label) > 0 {
		title += " (" + game.label + ")"
	}

	sdl.Init()
	defer sdl.Quit()

	win := sdl.CreateWindow("Williams - " + title, *scale * 304, *scale * 256, *fullscreen)
	defer win.Destroy()

	renderer := win.CreateRenderer()
	texture := renderer.CreateTexture(304, 256)

	// bzz := func (buffer []byte) {
	// 	for i := 0; i < len(buffer); i++ {
	// 		buffer[i] = byte(i)
	// 	}
	// }
	gap := func (buffer []byte) {
		game.audio.play(buffer)
	}
	if *enableaudio {
		game.audio.deviceid = sdl.OpenAudioDevice(&gap)
		defer sdl.CloseAudioDevice(game.audio.deviceid)
	}

	frame_count := 0
	paused := false
	last_frame := sdl.GetTicks()
	first_frame := last_frame
	msec_per_frame := 1000 / 60

	const ESC = 27

	for running := true; running; {

		var event sdl.Event
		for sdl.PollEvent(&event) {
			switch event.Type() {
				case sdl.QUIT:
					running = false
				case sdl.KEYDOWN, sdl.KEYUP:
					e := event.KeyboardEvent()
					switch e.Sym {
						case ESC:
							running = false
						case 'p':
							if e.Down {
								paused = !paused
							}
						default:
							game.HandleKey(e.Down, e.Sym)
					}
			}

		}

		if !paused {
			game.RunFrame()
			pixels, pitch := texture.Lock()
			Render(game.main.Ram, pixels, pitch)
			texture.Unlock()
			// renderer.Clear()
			renderer.CopyTexture(texture)
			renderer.Present()
		}

		var elapsed = sdl.GetTicks() - last_frame
		var remaining = msec_per_frame - int(elapsed)
		// fmt.Println("REMAINING", remaining, "ELAPSED", elapsed)
		if remaining < 1 { remaining = 1 }  // just to delay at least a little
		sdl.Delay(uint(remaining))
		last_frame = sdl.GetTicks()
		frame_count += 1
	}

	real_time := float64(sdl.GetTicks() - first_frame) / 1000.0
	fmt.Printf("Bye. %d frames went by.\n", frame_count)
	fmt.Printf("Effective CPU speed: %f%% / %f%%\n",
			   100 * game.main.Clock() / real_time,
			   100 * game.sound.Clock() / real_time)
}
